package com.tructuanguy.callereditor

import android.app.Application
import com.tructuanguy.callereditor.helper.createNotificationChannels
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CallerEditorApp : Application() {

    override fun onCreate() {
        super.onCreate()
        createNotificationChannels()
    }

}