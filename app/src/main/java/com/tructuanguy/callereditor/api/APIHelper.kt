package com.tructuanguy.callereditor.api

import javax.inject.Inject

class APIHelper @Inject constructor(private val apiService: APIService) {
    suspend fun getStyle() = apiService.getStyle()
}