package com.tructuanguy.callereditor.api

import com.tructuanguy.callereditor.api.model.APIResponse
import retrofit2.Response
import retrofit2.http.GET


const val BASE_URL = "http://s1.picsjoin.com"
const val PATH = "Material_library/public/V1/ColorPhone/getGroupFlashVideo?statue=2&&music=1"

interface APIService {
    @GET(PATH)
    suspend fun getStyle(): Response<APIResponse>
}