package com.tructuanguy.callereditor.api.model

data class APIResponse(
    val data: List<DataRemote>,
    val msg: String,
    val server_time: Int,
    val status: Int
)