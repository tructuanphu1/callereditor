package com.tructuanguy.callereditor.api.model

import com.google.gson.annotations.SerializedName

data class ConfRemote(
    val g_id: Int,
    val index: Int,
    val is_hot: Int,
    val is_lock: Int,
    val is_new: Int,
    val is_rec: Int,
    val is_voice: Int,
    val like: Int,
    val material: ScreenRemote,
    @SerializedName("uniqid")
    val unique: String
)