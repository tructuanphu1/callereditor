package com.tructuanguy.callereditor.api.model

import com.tructuanguy.callereditor.api.model.ConfRemote

data class DataRemote(
    val conf: List<ConfRemote>,
    val desc: String,
    val icon: String,
    val id: Int,
    val index: Int,
    val name: String
)