package com.tructuanguy.callereditor.api.model

data class ScreenRemote(
    val big_image_url: String,
    val desc: String,
    val id: Int,
    val is_outside: Int,
    val item_name: String,
    val out_big_image: String,
    val small_image_url: String,
    val video_url: String
)