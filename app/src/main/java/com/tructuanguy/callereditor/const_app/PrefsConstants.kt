package com.tructuanguy.callereditor.const_app

object PrefsConstants {
    const val PREF_VIBRATION = "vibration"
    const val PREF_FLASH = "flash"
    const val MY_PREFS = "my_preference"
    const val LANGUAGE = "language"
}