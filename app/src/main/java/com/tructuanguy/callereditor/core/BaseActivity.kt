package com.tructuanguy.callereditor.core

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.extensions.requestDetailSetting

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    protected lateinit var binding: VB
    private var loadingDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getLayout()
        setContentView(binding.root)
        setUpLoadingDialog()
        initViews()
    }

    private fun setUpLoadingDialog() {
        loadingDialog = Dialog(this)
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_loading, null, false)
        loadingDialog?.window?.setBackgroundDrawable(ColorDrawable(resources.getColor(android.R.color.transparent, null)))
        loadingDialog?.setCancelable(false)
        loadingDialog?.setContentView(view)
    }

    fun showRequestDetailSettingsDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_change_permission)

        val window = dialog.window ?: return
        window.apply {
            setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val windowAttrs = window.attributes
        windowAttrs.gravity = Gravity.CENTER
        dialog.setCancelable(false)
        dialog.show()

        val cancel = dialog.findViewById<TextView>(R.id.cancel)
        val goToSetting = dialog.findViewById<TextView>(R.id.go_to_settings)
        cancel.setOnClickListener {
            dialog.dismiss()
        }
        goToSetting.setOnClickListener {
            requestDetailSetting()
            dialog.dismiss()
        }
    }

    fun showLoadingDialog() {
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.hide()
    }

    abstract fun initViews()

    abstract fun getLayout(): VB


}