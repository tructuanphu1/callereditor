package com.tructuanguy.callereditor.core

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.tructuanguy.callereditor.R
import javax.inject.Inject

class BaseOptionDialog @Inject constructor(private val context: Context) : Dialog(context) {

    private lateinit var listener: OptionDialogListener
    private var title = ""
    private var message = ""
    private var negative = ""
    private var positive = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_option)
    }

    fun setOptionListener(listener: OptionDialogListener) {
        this.listener = listener
    }

    fun setOptionTitleValue(value: String) {
        this.title = value
    }

    fun setOptionMessageValue(value: String) {
        this.message = value
    }

    fun setOptionNegativeValue(value: String) {
        this.negative = value
    }

    fun setOptionPositiveValue(value: String) {
        this.positive = value
    }


    override fun show() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_option)

        val window = dialog.window ?: return
        window.apply {
            setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val windowAttrs = window.attributes
        windowAttrs.gravity = Gravity.CENTER

        val negative = dialog.findViewById<TextView>(R.id.negative)
        val title = dialog.findViewById<TextView>(R.id.title)
        val message = dialog.findViewById<TextView>(R.id.message)
        val positive = dialog.findViewById<TextView>(R.id.positive)

        title.text = this.title
        message.text = this.message
        negative.text = this.negative
        positive.text = this.positive


        negative.setOnClickListener {
            listener.onNegative()
            dialog.dismiss()
        }
        positive.setOnClickListener {
            listener.onPositive()
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }
}