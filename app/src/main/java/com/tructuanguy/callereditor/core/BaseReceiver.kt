package com.tructuanguy.callereditor.core

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.annotation.CallSuper
import com.tructuanguy.callereditor.CallerEditorApp

abstract class BaseReceiver : BroadcastReceiver() {

    @CallSuper
    override fun onReceive(context: Context?, intent: Intent?) {
        val baseApp = context?.applicationContext as CallerEditorApp
    }

}