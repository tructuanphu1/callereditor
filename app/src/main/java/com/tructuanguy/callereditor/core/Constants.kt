package com.tructuanguy.callereditor.core

object Constants {
    const val IS_FIRST_IN_APP = "is_first_in_app"
    const val MY_BACKGROUND = "my_background"
    const val URI_PATH = "content://media/external/images/media/"
    const val REQUEST_PERMISSION_CODE = 2610
    const val INTENT_ACTION_CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE"
    const val CHOOSE_REMOTE_BACKGROUND = "choose_remote_background"
    const val CHOOSE_LOCAL_BACKGROUND = "choose_local_background"
    const val CHOOSE_AVATAR = "choose_avatar"
    const val CHOOSE_ACCEPT_ICON = "choose_accept_avatar"
    const val CHOOSE_DECLINE_ICON = "choose_decline_avatar"
    const val CHOSEN_AUDIO_PATH = "audio_path"
    const val CHOSEN_AUDIO_NAME = "audio_name"
    const val URI_SIGNAL = "content://media/external/images/media/"
    const val DIALER_BACKGROUND = "dialer_background"
    const val DIALER_AVATAR = "dialer_avatar"
    const val DIALER_ACCEPT = "dialer_accept"
    const val DIALER_DECLINE = "dialer_decline"
    const val DIALER_AUDIO_PATH = "dialer_audio_path"
    const val DIALER_AUDIO_NAME = "dialer_audio_name"

    const val AVATAR_ASSET_PATH = "Images/avatar"
    const val ACCEPT_ASSET_PATH = "Images/accept"
    const val DECLINE_ASSET_PATH = "Images/decline"
    const val BACKGROUND_ASSET_PATH = "Images/background"
    const val APP_PATH = "com.tructuanguy.callereditor/"
    const val URL_PATH = "http://s1.picsjoin.com"

    const val ACCEPT_CALL = "accept_call"
    const val DECLINE_CALL = "decline_call"
    const val REQUEST_DEFAULT_DIALER_CODE = 666

}