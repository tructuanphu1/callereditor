package com.tructuanguy.callereditor.core

interface OnItemClickListener {
    fun onItemClick(data: Any)
}