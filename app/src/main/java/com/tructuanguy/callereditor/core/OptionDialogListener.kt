package com.tructuanguy.callereditor.core

interface OptionDialogListener {
    fun onNegative()
    fun onPositive()
}