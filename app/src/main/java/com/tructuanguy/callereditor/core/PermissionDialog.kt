package com.tructuanguy.callereditor.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.databinding.DialogChangePermissionBinding

class PermissionDialog : DialogFragment() {

    private lateinit var binding: DialogChangePermissionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.full_screen_dialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogChangePermissionBinding.inflate(layoutInflater)
        return binding.root
    }

}