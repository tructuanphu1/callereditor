package com.tructuanguy.callereditor.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.db.dao.CallerDao
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.db.entities.ImageDevice

@Database(entities = [ImageDevice::class, Avatar::class, DialerScreen::class], version = 1)
abstract class CallerDatabase : RoomDatabase() {

    abstract fun getCallerDao(): CallerDao

    companion object {
        const val DATABASE_NAME = "caller_db"
    }

}