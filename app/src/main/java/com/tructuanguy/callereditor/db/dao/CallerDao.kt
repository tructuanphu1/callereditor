package com.tructuanguy.callereditor.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.db.entities.ImageDevice
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface CallerDao : BaseDao<ImageDevice>{

    @Query("select * from ImageDevice")
    fun getAllImagesDevice(): List<ImageDevice>

    @Query("delete from ImageDevice where id = :id")
    fun deleteImageDevice(id: Int): Completable

    @Query("select * from Avatar")
    fun getAllAvatarDevice(): List<Avatar>

    @Query("delete from Avatar where id = :id")
    fun deleteAvatarDevice(id: Int): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAvatarDevice(avatar: Avatar): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDialerScreen(screen: DialerScreen): Single<Long>

    @Query("delete from DialerScreen where id = :id")
    fun deleteDialerScreen(id: Int): Completable

    @Query("select * from DialerScreen")
    fun getAllDialerScreen(): List<DialerScreen>

}