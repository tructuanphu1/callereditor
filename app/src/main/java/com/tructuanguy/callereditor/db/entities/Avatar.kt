package com.tructuanguy.callereditor.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Avatar (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val path: String
)