package com.tructuanguy.callereditor.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class DialerScreen (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val background: String,
    val avatar: String,
    val accept: String,
    val decline: String,
    val audioPath: String,
    val audioName: String
): Serializable