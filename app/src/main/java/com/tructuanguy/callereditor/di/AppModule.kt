package com.tructuanguy.callereditor.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.AssetManager
import androidx.core.app.NotificationManagerCompat
import androidx.room.Room
import com.tructuanguy.callereditor.api.APIService
import com.tructuanguy.callereditor.api.BASE_URL
import com.tructuanguy.callereditor.const_app.PrefsConstants.MY_PREFS
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.db.dao.CallerDao
import com.tructuanguy.callereditor.helper.ImageHelper
import com.tructuanguy.callereditor.helper.PermissionHelper
import com.tructuanguy.callereditor.helper.PreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun providePrefsHelper(mPrefs: SharedPreferences) = PreferencesHelper(mPrefs)

    @Singleton
    @Provides
    fun providePermissionHelper(context: Context) = PermissionHelper(context)

    @Singleton
    @Provides
    fun provideImageHelpers(context: Context) = ImageHelper(context)

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideCallerDatabase(context: Context) : CallerDatabase {
        return Room.databaseBuilder(context, CallerDatabase::class.java, CallerDatabase.DATABASE_NAME).allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun provideCallerDao(db: CallerDatabase): CallerDao {
        return db.getCallerDao()
    }

    @Provides
    fun provideAPIService(retrofit: Retrofit): APIService = retrofit.create(APIService::class.java)

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun okHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    fun provideAssetManager(context: Context): AssetManager {
        return context.assets
    }

    @Singleton
    @Provides
    fun notificationManager(context: Context) = NotificationManagerCompat.from(context)

}