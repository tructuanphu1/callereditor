package com.tructuanguy.callereditor.di

import com.tructuanguy.callereditor.feature.audio.AudioContract
import com.tructuanguy.callereditor.feature.audio.AudioPresenter
import com.tructuanguy.callereditor.feature.background.BackgroundContract
import com.tructuanguy.callereditor.feature.background.BackgroundPresenter
import com.tructuanguy.callereditor.feature.dialer.DialerContract
import com.tructuanguy.callereditor.feature.dialer.DialerPresenter
import com.tructuanguy.callereditor.feature.edit.EditContract
import com.tructuanguy.callereditor.feature.edit.EditPresenter
import com.tructuanguy.callereditor.feature.icon_avatar.IconAndAvatarContract
import com.tructuanguy.callereditor.feature.icon_avatar.IconAndAvatarPresenter
import com.tructuanguy.callereditor.feature.introduce.IntroduceContract
import com.tructuanguy.callereditor.feature.introduce.IntroducePresenter
import com.tructuanguy.callereditor.feature.language.LanguageContract
import com.tructuanguy.callereditor.feature.language.LanguagePresenter
import com.tructuanguy.callereditor.feature.preview.PreviewContract
import com.tructuanguy.callereditor.feature.preview.PreviewPresenter
import com.tructuanguy.callereditor.feature.setting.SettingContract
import com.tructuanguy.callereditor.feature.setting.SettingPresenter
import com.tructuanguy.callereditor.feature.splash.SplashContract
import com.tructuanguy.callereditor.feature.splash.SplashPresenter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class PresenterModule {

    @Binds
    abstract fun providesEditPresenter(presenter: EditPresenter): EditContract.Presenter

    @Binds
    abstract fun providesSettingPresenter(presenter: SettingPresenter): SettingContract.Presenter

    @Binds
    abstract fun provideBackgroundPresenter(presenter: BackgroundPresenter): BackgroundContract.Presenter

    @Binds
    abstract fun provideIconAndAvatarPresenter(presenter: IconAndAvatarPresenter): IconAndAvatarContract.Presenter

    @Binds
    abstract fun provideAudioPresenter(presenter: AudioPresenter): AudioContract.Presenter

    @Binds
    abstract fun providePreviewPresenter(presenter: PreviewPresenter): PreviewContract.Presenter

    @Binds
    abstract fun provideIntroducePresenter(presenter: IntroducePresenter): IntroduceContract.Presenter

    @Binds
    abstract fun provideSplashPresenter(presenter: SplashPresenter): SplashContract.Presenter

    @Binds
    abstract fun provideDialerPresenter(presenter: DialerPresenter): DialerContract.Presenter

    @Binds
    abstract fun provideLanguagePresenter(presenter: LanguagePresenter): LanguageContract.Presenter

}