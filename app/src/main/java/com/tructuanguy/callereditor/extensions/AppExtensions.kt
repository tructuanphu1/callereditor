package com.tructuanguy.callereditor.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.Date

fun Context.getBitmapFromAsset(folder: String, name: String): Bitmap {
    val assetManager = this.assets
    var inputStream: InputStream? = null
    try {
        inputStream = assetManager.open("$folder/$name")
        Log.i("hhh","$folder/$name")
    } catch (e: IOException) {
        e.printStackTrace()
        Log.i("cc", e.toString())
    }
    return BitmapFactory.decodeStream(inputStream)
}

fun Context.getBitmapFromAsset(path: String): Bitmap {
    val assetManager = this.assets
    var inputStream: InputStream? = null
    try {
        inputStream = assetManager.open(path)
    } catch (e: IOException) {
        e.printStackTrace()
        Log.i("cc", e.toString())
    }
    return BitmapFactory.decodeStream(inputStream)
}

@SuppressLint("SimpleDateFormat")
fun Context.saveBitmap(bitmap: Bitmap): String {
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    try {
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        val fos = FileOutputStream(imageFile)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        fos.close()
        Log.i("bitmap", "Saved to " + imageFile.absolutePath)
        return imageFile.absolutePath
    } catch (e: Exception) {
        Log.e("ioe", e.toString())
    }
    throw Exception("!!!!!!!!!!!")
}
