package com.tructuanguy.callereditor.extensions

import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.TextView

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun TextView.animateRightToLeft() {
    val textWidth = this.paint.measureText(this.text.toString())
    val animator = ObjectAnimator.ofFloat(this, "translationX", -textWidth, textWidth)
    animator.apply {
        interpolator = LinearInterpolator()
        duration = 5000
        repeatCount = ObjectAnimator.INFINITE
        addUpdateListener {
            val value = it.animatedValue as Float
            this@animateRightToLeft.translationX = value
        }
        start()
    }
}

fun ImageView.ring() {
    this.animate()
        .translationY(-100f)
        .setDuration(300)
        .withStartAction {
        }
        .withEndAction {
            this.postDelayed({
                this.animate()
                    .translationY(0f) // Đặt lại vị trí ban đầu
                    .setDuration(300)
                    .withStartAction {
                    }
                    .withEndAction {
                        ring()
                    }
                    .start()
            }, 250)
        }
        .start()
}