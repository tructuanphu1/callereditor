package com.tructuanguy.callereditor.feature.audio

import android.content.Intent
import android.os.Bundle
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.databinding.ActivityAudioBinding
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.feature.audio.adapter.Audio
import com.tructuanguy.callereditor.feature.audio.adapter.AudioAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val CHOSEN_AUDIO = 666

@AndroidEntryPoint
class AudioActivity : BaseActivity<ActivityAudioBinding>(), AudioContract.View {

    @Inject
    lateinit var mPresenter: AudioPresenter

    override fun initViews() {
        binding.back.setOnClickListener {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.applyAudioDevice()
    }

    override fun getLayout(): ActivityAudioBinding {
        return ActivityAudioBinding.inflate(layoutInflater)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

    override fun updateAudioAdapter(listAudios: ArrayList<Audio>) {
        val handleClick: (Audio) -> Unit = { audio ->
            val intent = Intent()
            intent.putExtra(Constants.CHOSEN_AUDIO_PATH, audio.path)
            intent.putExtra(Constants.CHOSEN_AUDIO_NAME, audio.name)
            setResult(CHOSEN_AUDIO, intent)
            finish()
        }
        val adapter = AudioAdapter(listAudios, handleClick)
        binding.rcvAudio.adapter = adapter
    }

    override fun showEmpty() {
        binding.rcvAudio.gone()
        binding.animEmpty.visible()
        binding.tvNotifyEmpty.visible()
    }

    override fun hideEmpty() {
        binding.rcvAudio.visible()
        binding.animEmpty.gone()
        binding.tvNotifyEmpty.gone()
    }

}