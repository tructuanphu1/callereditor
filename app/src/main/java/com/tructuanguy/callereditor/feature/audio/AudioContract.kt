package com.tructuanguy.callereditor.feature.audio

import android.content.Intent
import com.tructuanguy.callereditor.core.ActivityContract
import com.tructuanguy.callereditor.feature.audio.adapter.Audio

interface AudioContract {

    interface View: ActivityContract.View {
        fun updateAudioAdapter(listAudios: ArrayList<Audio>)
        fun showEmpty()
        fun hideEmpty()
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun getAllDeviceAudio(): ArrayList<Audio>
        fun applyAudioDevice()
    }

}