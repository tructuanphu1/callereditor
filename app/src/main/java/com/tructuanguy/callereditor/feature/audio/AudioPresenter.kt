package com.tructuanguy.callereditor.feature.audio

import android.content.Context
import android.content.Intent
import android.provider.MediaStore
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.feature.audio.adapter.Audio
import javax.inject.Inject

class AudioPresenter @Inject constructor(
    private val context: Context
) : AudioContract.Presenter {

    private var mView: AudioContract.View? = null

    override fun getAllDeviceAudio(): ArrayList<Audio> {
        val listAudio: ArrayList<Audio> = arrayListOf()
        val contentResolver = context.contentResolver
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.DISPLAY_NAME, MediaStore.Audio.AudioColumns.DURATION)
        val cursor = contentResolver.query(uri, projection, null, null, null)

        cursor?.use {
            val dataIndex = it.getColumnIndexOrThrow(MediaStore.Audio.AudioColumns.DATA)
            val nameIndex = it.getColumnIndexOrThrow(MediaStore.Audio.AudioColumns.DISPLAY_NAME)
            val durationIndex = it.getColumnIndexOrThrow(MediaStore.Audio.AudioColumns.DURATION)
            while (it.moveToNext()) {
                val filePath = it.getString(dataIndex)
                val fileName = it.getString(nameIndex)
                val fileDuration = it.getInt(durationIndex)
                listAudio.add(Audio(filePath, fileName, fileDuration))

            }
        }
        cursor?.close()
        return listAudio
    }

    override fun applyAudioDevice() {
        val listAudio = getAllDeviceAudio()
        if(listAudio.size > 0) {
            mView?.hideEmpty()
            mView?.updateAudioAdapter(listAudio)
        } else {
            mView?.showEmpty()
        }
    }


    override fun attach(view: AudioContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}