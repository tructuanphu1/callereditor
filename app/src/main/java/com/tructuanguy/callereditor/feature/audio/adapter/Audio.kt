package com.tructuanguy.callereditor.feature.audio.adapter

data class Audio(
    val path: String,
    val name: String,
    val duration: Int
)
