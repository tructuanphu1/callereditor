package com.tructuanguy.callereditor.feature.audio.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.databinding.ItemAudioBinding

class AudioAdapter(
    private val list: ArrayList<Audio>,
    private val listener: (Audio) -> Unit
) : RecyclerView.Adapter<AudioAdapter.AudioViewHolder>() {

    inner class AudioViewHolder(private val binding: ItemAudioBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(audio: Audio) {
            binding.apply {
                tvAudioName.text = audio.name
                tvAudioDuration.text = convertDuration((audio.duration).toLong())
                if(adapterPosition % 2 == 0) {
                    cvAudio.startAnimation(AnimationUtils.loadAnimation(itemView.context, R.anim.slide_right))
                } else {
                    cvAudio.startAnimation(AnimationUtils.loadAnimation(itemView.context, R.anim.slide_left))
                }
            }
            itemView.setOnClickListener {
                listener.invoke(audio)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioViewHolder {
        return AudioViewHolder(ItemAudioBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

fun convertDuration(duration: Long): String {
    val minute: Long = (duration / 1000) / 60
    val second: Long = (duration / 1000) % 60
    if(minute < 10 && second < 10) {
        return "0$minute:0$second"
    } else if(minute < 10 && second > 10) {
        return "0$minute:$second"
    } else if(minute > 10 && second > 10) {
        return "$minute:$second"
    } else if(minute > 10 && second < 10) {
        return "$minute:0$second"
    }
    return ""
}