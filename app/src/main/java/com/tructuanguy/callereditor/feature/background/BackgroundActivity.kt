package com.tructuanguy.callereditor.feature.background

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.api.model.ConfRemote
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.BaseOptionDialog
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.core.OptionDialogListener
import com.tructuanguy.callereditor.extensions.getLaunchGalleryIntent
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.databinding.ActivityBackgroundBinding
import com.tructuanguy.callereditor.db.entities.ImageDevice
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.feature.background.adapter.BackgroundAdapter
import com.tructuanguy.callereditor.feature.background.adapter.RemoteBackgroundAdapter
import com.tructuanguy.callereditor.helper.PermissionHelper
import com.tructuanguy.callereditor.helper.network.NetworkStatusCallback
import com.tructuanguy.callereditor.helper.network.NetworkStatusReceiver
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val CHOOSE_REMOTE_BACKGROUND = 110
const val CHOOSE_LOCAL_BACKGROUND = 111

@AndroidEntryPoint
class BackgroundActivity : BaseActivity<ActivityBackgroundBinding>(), BackgroundContract.View {

    private lateinit var internetCallback: NetworkStatusReceiver

    @Inject
    lateinit var mPresenter: BackgroundContract.Presenter

    @Inject
    lateinit var mPermissionHelper: PermissionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.applyImagesDevice()
    }

    override fun initViews() {
        initBroadcastReceiver()
        binding.apply {
            icBack.setOnClickListener {
                onBackPressedDispatcher.onBackPressed()
            }
            cvAdd.setOnClickListener {
                if(mPermissionHelper.isStoragePermissionGranted()) {
                    pickImage.launch(getLaunchGalleryIntent())
                } else {
                    requestPermissions(arrayOf(
                        if(mPermissionHelper.isAndroid13()) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), Constants.REQUEST_PERMISSION_CODE)
                }
            }
            setInternetStatusCallback(object : NetworkStatusCallback {
                override fun onStatusChange(isConnected: Boolean) {
                    if(isConnected) {
                        runOnUiThread {
                            rcvBackground.visible()
                            cvInternet.gone()
                            mPresenter.reloadTheme()
                        }
                    } else {
                        runOnUiThread {
                            rcvBackground.gone()
                            cvInternet.visible()
                        }
                    }
                }
            })
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Constants.REQUEST_PERMISSION_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage.launch(getLaunchGalleryIntent())
        } else {
            showRequestDetailSettingsDialog()
        }
    }

    private val pickImage = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if(result.resultCode == RESULT_OK) {
            val uri = result.data!!.data
            val bitmap = mPresenter.getBitmapFromUri(uri.toString())
            mPresenter.saveBitmap(bitmap)
            mPresenter.insertImageDevice(ImageDevice(0, mPresenter.getBitmapPath(bitmap)))
        }
    }

    override fun getLayout(): ActivityBackgroundBinding =
        ActivityBackgroundBinding.inflate(layoutInflater)

    @SuppressLint("NotifyDataSetChanged")
    override fun updateMyBackgroundAdapter(list: List<ImageDevice>) {
        lateinit var adapter: BackgroundAdapter
        val handleClick: (Int, ImageDevice) -> Unit = { _, imageDevice ->
            val intent = Intent()
            intent.putExtra(Constants.CHOOSE_LOCAL_BACKGROUND, imageDevice.path)
            setResult(CHOOSE_LOCAL_BACKGROUND, intent)
            finish()
        }
        val handleLongClick: (Int, ImageDevice) -> Unit = { pos, image ->
            run {
                val dialog = BaseOptionDialog(this@BackgroundActivity)
                dialog.apply {
                    setOptionTitleValue(getString(R.string.delete_saved_image_device_title))
                    setOptionMessageValue(getString(R.string.delete_saved_image_device_message))
                    setOptionNegativeValue(getString(R.string.cancel))
                    setOptionPositiveValue(getString(R.string.delete))
                }
                dialog.setOptionListener(object : OptionDialogListener {
                    override fun onNegative() {
                        // do nothing
                    }

                    override fun onPositive() {
                        mPresenter.deleteImageDevice(image.id)
                        adapter.notifyItemRemoved(pos)
                    }
                })
                dialog.show()
            }
        }
        adapter = BackgroundAdapter(clickListener = handleClick, longClickListener = handleLongClick)
        adapter.submitList(list)
        binding.rcvMyBackground.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun updateRemoteBackgroundAdapter(list: MutableList<ConfRemote>) {
        val handleClick: (Int, ConfRemote) -> Unit = { _, conf ->
            val intent = Intent()
            intent.putExtra(Constants.CHOOSE_REMOTE_BACKGROUND, conf.material.big_image_url)
            setResult(CHOOSE_REMOTE_BACKGROUND, intent)
            finish()
        }
        val adapter = RemoteBackgroundAdapter(clickListener = handleClick)
        adapter.submitList(list)
        binding.rcvBackground.adapter = adapter
    }

    override fun onInsertSuccess() {
        shortToast(getString(R.string.insert_successfully))
        mPresenter.applyImagesDevice()
    }

    override fun onDeleteSuccess() {
        shortToast(getString(R.string.delete_successfully))
        mPresenter.applyImagesDevice()
    }

    override fun showError(error: String) {
        Log.i("error", error)
    }

    override fun onCallApiError(error: String) {
        Log.i("hello", error)
        binding.apply {
            rcvBackground.gone()
            cvInternet.visible()
        }
    }

    private fun initBroadcastReceiver() {
        internetCallback = NetworkStatusReceiver()
        val filter = IntentFilter()
        filter.addAction(Constants.INTENT_ACTION_CONNECTIVITY_CHANGE)
        registerReceiver(internetCallback, filter)
    }

    private fun setInternetStatusCallback(internetCallback: NetworkStatusCallback) {
        this.internetCallback.initNetworkStatusCallback(internetCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}