package com.tructuanguy.callereditor.feature.background

import android.content.Intent
import android.graphics.Bitmap
import com.tructuanguy.callereditor.api.model.ConfRemote
import com.tructuanguy.callereditor.core.ActivityContract
import com.tructuanguy.callereditor.db.entities.ImageDevice

interface BackgroundContract {

    interface View: ActivityContract.View {
        fun updateMyBackgroundAdapter(list: List<ImageDevice>)
        fun updateRemoteBackgroundAdapter(list: MutableList<ConfRemote>)
        fun onInsertSuccess()
        fun onDeleteSuccess()
        fun showError(error: String)
        fun onCallApiError(error: String)
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun deleteImageDevice(id: Int)
        fun insertImageDevice(image: ImageDevice)
        fun getAllImagesDevice(): List<ImageDevice>
        fun applyImagesDevice()
        fun reloadTheme()
        fun getBitmapFromUri(uriPath: String): Bitmap
        fun saveBitmap(bitmap: Bitmap)
        fun getBitmapPath(bitmap: Bitmap): String
    }

}