package com.tructuanguy.callereditor.feature.background

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import com.tructuanguy.callereditor.api.APIHelper
import com.tructuanguy.callereditor.api.model.ConfRemote
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.db.entities.ImageDevice
import com.tructuanguy.callereditor.extensions.saveBitmap
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.Exception
import kotlin.coroutines.CoroutineContext


class BackgroundPresenter @Inject constructor(
    private val context: Context,
    private val callerDB: CallerDatabase,
    private val apiHelper: APIHelper
) : BackgroundContract.Presenter, CoroutineScope {

    private var mView: BackgroundContract.View? = null
    private val mListTheme = mutableListOf<ConfRemote>()

    override fun attach(view: BackgroundContract.View) {
        this.mView = view
        getTheme()
    }

    override fun detach() {
        mView = null
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun deleteImageDevice(id: Int) {
        callerDB.getCallerDao().deleteImageDevice(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    // nothing
                }

                override fun onComplete() {
                    mView?.onDeleteSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }

    override fun insertImageDevice(image: ImageDevice) {
        callerDB.getCallerDao().insert(image)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Long> {
                override fun onSubscribe(d: Disposable) {
                    // nothing
                }

                override fun onSuccess(t: Long) {
                    mView?.onInsertSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }

    override fun getAllImagesDevice(): List<ImageDevice> {
        try {
            return callerDB.getCallerDao().getAllImagesDevice()
        } catch (e: Exception) {
            mView?.showError(e.toString())
        }
        return emptyList()
    }

    override fun applyImagesDevice() {
        val listImage = getAllImagesDevice()
        launch {
            withContext(Dispatchers.Main) {
                mView?.updateMyBackgroundAdapter(listImage)
            }
        }
    }

    override fun reloadTheme() {
        getTheme()
    }

    @Suppress("DEPRECATION")
    override fun getBitmapFromUri(uriPath: String): Bitmap {
        return if(Build.VERSION.SDK_INT >= 29) {
            val source = ImageDecoder.createSource(context.contentResolver, Uri.parse(uriPath))
            ImageDecoder.decodeBitmap(source)
        } else {
            MediaStore.Images.Media.getBitmap(context.contentResolver, Uri.parse(uriPath))
        }
    }

    override fun saveBitmap(bitmap: Bitmap) {
        context.saveBitmap(bitmap)
    }

    override fun getBitmapPath(bitmap: Bitmap): String {
        return context.saveBitmap(bitmap)
    }


    private fun getTheme() {
        launch {
            try {
                val response = apiHelper.getStyle()
                response.body()?.run {
                    data.forEach {
                        mListTheme.addAll(it.conf)
                    }
                }
                withContext(Dispatchers.Main) {
                    mView?.updateRemoteBackgroundAdapter(mListTheme)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    mView?.onCallApiError(e.toString())
                }
            }
        }
    }

}