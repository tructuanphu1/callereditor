package com.tructuanguy.callereditor.feature.background.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.core.Constants.URI_PATH
import com.tructuanguy.callereditor.databinding.ItemBackgroundBinding
import com.tructuanguy.callereditor.db.entities.ImageDevice
import com.tructuanguy.callereditor.helper.ImageHelper

class BackgroundAdapter(
    private val clickListener: (Int, ImageDevice) -> Unit,
    private val longClickListener: (Int, ImageDevice) -> Unit
) : ListAdapter<ImageDevice, BackgroundAdapter.BackgroundViewHolder>(BackgroundDiffCallback()) {

    inner class BackgroundViewHolder(private val binding: ItemBackgroundBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(image: ImageDevice) {
            binding.apply {
                if (image.path.contains(URI_PATH)) {
                    ImageHelper(itemView.context).loadUri(image.path, ivBackground)
                } else {
                    ImageHelper(itemView.context).loadUrl(image.path, ivBackground)
                }
            }
            itemView.apply {
                setOnClickListener { clickListener.invoke(adapterPosition, image) }
                setOnLongClickListener {
                    longClickListener.invoke(adapterPosition, image)
                    true
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackgroundViewHolder {
        return BackgroundViewHolder(
            ItemBackgroundBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BackgroundViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}


class BackgroundDiffCallback : DiffUtil.ItemCallback<ImageDevice>() {
    override fun areItemsTheSame(oldItem: ImageDevice, newItem: ImageDevice): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ImageDevice, newItem: ImageDevice): Boolean {
        return oldItem == newItem
    }

}
