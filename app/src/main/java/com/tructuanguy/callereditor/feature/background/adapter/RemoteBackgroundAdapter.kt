package com.tructuanguy.callereditor.feature.background.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.api.model.ConfRemote
import com.tructuanguy.callereditor.databinding.ItemBackgroundBinding
import com.tructuanguy.callereditor.helper.ImageHelper

class RemoteBackgroundAdapter(
    private val clickListener: (Int, ConfRemote) -> Unit
) : ListAdapter<ConfRemote, RemoteBackgroundAdapter.RemoteBackgroundViewHolder>(
    RemoteBackgroundDiffCallback()
) {

    inner class RemoteBackgroundViewHolder(private val binding: ItemBackgroundBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(conf: ConfRemote) {
            binding.apply {
                ImageHelper(itemView.context).loadUrl(conf.material.big_image_url, ivBackground)
            }
            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition, conf)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RemoteBackgroundViewHolder {
        return RemoteBackgroundViewHolder(
            ItemBackgroundBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RemoteBackgroundViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}


class RemoteBackgroundDiffCallback : DiffUtil.ItemCallback<ConfRemote>() {
    override fun areItemsTheSame(oldItem: ConfRemote, newItem: ConfRemote): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ConfRemote, newItem: ConfRemote): Boolean {
        return oldItem == newItem
    }
}


