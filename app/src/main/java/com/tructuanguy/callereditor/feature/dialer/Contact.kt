package com.tructuanguy.callereditor.feature.dialer

data class Contact (
    val name: String,
    val number: String,
    val avatar: String,
    val accept: String,
    val decline: String
)