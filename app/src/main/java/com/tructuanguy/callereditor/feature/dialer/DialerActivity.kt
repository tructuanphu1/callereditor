package com.tructuanguy.callereditor.feature.dialer

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.databinding.ActivityDialerBinding
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.ring
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.helper.FlashHelper
import com.tructuanguy.callereditor.helper.ImageHelper
import com.tructuanguy.callereditor.helper.NotificationHelper
import com.tructuanguy.callereditor.helper.OnCalling
import com.tructuanguy.callereditor.helper.VibrateHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DialerActivity : BaseActivity<ActivityDialerBinding>(), DialerContract.View {

    @Inject
    lateinit var mPresenter: DialerPresenter

    @Inject
    lateinit var mNotificationHelper: NotificationHelper

    @Inject
    lateinit var mFlashHelper: FlashHelper

    @Inject
    lateinit var mVibrateHelper: VibrateHelper

    private var phoneNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.subscribeUpdateUI()
    }

    override fun initViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            this.window.addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            )
        }
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        mPresenter.disconnectCall()

        binding.apply {
            accept.ring()
            decline.ring()
            accept.setOnClickListener {
                OnCalling.accept()
                turnOffFlashAndVibrate()
                hideView()
            }
            decline.setOnClickListener {
                turnOffFlashAndVibrate()
                OnCalling.decline()
            }
        }
    }

    override fun getLayout(): ActivityDialerBinding {
        return ActivityDialerBinding.inflate(layoutInflater)
    }

    override fun getPhoneNumber(): String {
        phoneNumber = intent.data?.schemeSpecificPart.toString()
        return phoneNumber
    }

    override fun displayPhoneNumber(number: String) {
        binding.phoneNumber.text = number
    }

    override fun displayName(name: String) {
        binding.tvName.text = name
    }

    override fun displayBackgroundAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(path), binding.ivBackground)
    }

    override fun displayBackgroundUser(path: String) {
        ImageHelper(this).loadUri(path, binding.ivBackground)
    }

    override fun displayAvatarAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(AVATAR_ASSET_PATH, path), binding.ivAvatar)
    }

    override fun displayAvatarUser(path: String) {
        ImageHelper(this).loadUri(path, binding.ivAvatar)
    }

    override fun displayAccept(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(ACCEPT_ASSET_PATH, path), binding.accept)
    }

    override fun displayDecline(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(DECLINE_ASSET_PATH, path), binding.decline)
    }

    override fun showAnswerButton() {
        binding.accept.visible()
    }

    override fun hideAnswerButton() {
        binding.accept.gone()
    }

    override fun showDeclineButton() {
        binding.decline.visible()
    }

    override fun hideDeclineButton() {
        binding.decline.gone()
    }

    override fun onFinish() {
        finish()
    }

    override fun ringing(
        name: String,
        number: String,
        avatar: String,
        accept: String,
        decline: String
    ) {
        mNotificationHelper.showDialerNotification(name, number, avatar, accept, decline)
        if (mPresenter.isFlashOn() && mPresenter.isVibrateOn()) {
            mFlashHelper.flashOn()
            mVibrateHelper.vibrateOn()
        } else if(mPresenter.isFlashOn() && !mPresenter.isVibrateOn()) {
            mFlashHelper.flashOn()
        } else if(!mPresenter.isFlashOn() && mPresenter.isVibrateOn()) {
            mVibrateHelper.vibrateOn()
        }
    }

    override fun pickUpped(
        name: String,
        number: String,
        avatar: String,
        accept: String,
        decline: String
    ) {
        hideView()
        mNotificationHelper.showReceivedDialer(name, number, avatar, accept, decline)
        turnOffFlashAndVibrate()
    }

    override fun disconnect(missingCall: Boolean, name: String, number: String, id: Long) {
        mNotificationHelper.clearComingCallNotification()
        turnOffFlashAndVibrate()
        finish()
    }

    override fun dialing(
        name: String,
        number: String,
        avatar: String,
        accept: String,
        decline: String
    ) {
        hideView()
    }

    private fun hideView() {
        binding.apply {
            accept.gone()
            decline.gone()
            decline2.visible()
        }
    }

    override fun onStop() {
        super.onStop()
        mPresenter.clearDisposables()
    }

    private fun turnOffFlashAndVibrate() {
        mFlashHelper.flashOff()
        mVibrateHelper.vibrateOff()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}