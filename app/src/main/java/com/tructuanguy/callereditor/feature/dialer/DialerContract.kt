package com.tructuanguy.callereditor.feature.dialer

import com.tructuanguy.callereditor.core.ActivityContract

interface DialerContract {

    interface View: ActivityContract.View {
        fun getPhoneNumber(): String
        fun displayBackgroundAsset(path: String)
        fun displayBackgroundUser(path: String)
        fun displayAvatarAsset(path: String)
        fun displayAvatarUser(path: String)
        fun displayAccept(path: String)
        fun displayDecline(path: String)
        fun displayPhoneNumber(number: String)
        fun displayName(name: String)

        fun showAnswerButton()
        fun hideAnswerButton()
        fun showDeclineButton()
        fun hideDeclineButton()
        fun onFinish()
        fun ringing(name: String, number: String, avatar: String, accept: String, decline: String)
        fun pickUpped(name: String, number: String, avatar: String, accept: String, decline: String)
        fun disconnect(missingCall: Boolean, name: String, number: String, id: Long)
        fun dialing(name: String, number: String, avatar: String, accept: String, decline: String)
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun subscribeUpdateUI()
        fun clearDisposables()
        fun disconnectCall()
        fun isFlashOn(): Boolean
        fun isVibrateOn(): Boolean
    }

}