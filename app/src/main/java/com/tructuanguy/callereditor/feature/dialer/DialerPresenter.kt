package com.tructuanguy.callereditor.feature.dialer


import android.telecom.Call
import com.tructuanguy.callereditor.const_app.PrefsConstants.PREF_FLASH
import com.tructuanguy.callereditor.const_app.PrefsConstants.PREF_VIBRATION
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_ACCEPT
import com.tructuanguy.callereditor.core.Constants.DIALER_AVATAR
import com.tructuanguy.callereditor.core.Constants.DIALER_BACKGROUND
import com.tructuanguy.callereditor.core.Constants.DIALER_DECLINE
import com.tructuanguy.callereditor.helper.CallHelper
import com.tructuanguy.callereditor.helper.OnCalling
import com.tructuanguy.callereditor.helper.PreferencesHelper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DialerPresenter @Inject constructor(
    private val mPrefs: PreferencesHelper,
    private val callHelper: CallHelper
) : DialerContract.Presenter, Call.Callback() {

    private var mView: DialerContract.View? = null
    private val disposables = CompositeDisposable()
    private var reject = false
    private var active = false


    override fun subscribeUpdateUI() {
        OnCalling.state.subscribe(::updateUI).addTo(disposables)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun disconnectCall() {
        OnCalling.state
            .filter { it == Call.STATE_DISCONNECTED }
            .delay(1, TimeUnit.SECONDS)
            .firstElement()
            .subscribe { mView?.onFinish() }
            .addTo(disposables)
    }

    override fun isFlashOn(): Boolean {
        return mPrefs.getBoolean(PREF_FLASH)
    }

    override fun isVibrateOn(): Boolean {
        return mPrefs.getBoolean(PREF_VIBRATION)
    }


    override fun attach(view: DialerContract.View) {
        mView = view
        OnCalling.state
    }

    private fun dialing(name: String, number: String, avatar: String, accept: String, decline: String) {
        mView?.dialing(name, number, avatar, accept, decline)
    }

    private fun active(name: String, number: String, avatar: String, accept: String, decline: String) {
        active = true
        mView?.pickUpped(name, number, avatar, accept, decline)
    }

    private fun ringing(name: String, number: String, avatar: String, accept: String, decline: String) {
        reject = false
        active = false
        mView?.ringing(name, number, avatar, accept, decline)
    }

    private fun updateUI(state: Int) {
        val background = mPrefs.getString(DIALER_BACKGROUND).toString()
        val avatar = mPrefs.getString(DIALER_AVATAR).toString()
        val accept = mPrefs.getString(DIALER_ACCEPT).toString()
        val decline = mPrefs.getString(DIALER_DECLINE).toString()
        val phoneNumber = mView?.getPhoneNumber().toString()
        val name = callHelper.getNameByNumber(phoneNumber)
        mView?.displayPhoneNumber(phoneNumber)
        mView?.displayName(name)
        if(background.contains(APP_PATH)) mView?.displayBackgroundUser(background) else mView?.displayBackgroundAsset(background)
        if(avatar.contains(APP_PATH)) mView?.displayAvatarUser(avatar) else mView?.displayAvatarAsset(avatar)
        mView?.displayAccept(accept)
        mView?.displayDecline(decline)
        if(state == Call.STATE_RINGING) mView?.showAnswerButton() else mView?.hideAnswerButton()
        if(state in listOf(Call.STATE_DIALING, Call.STATE_ACTIVE, Call.STATE_RINGING)) mView?.showDeclineButton() else mView?.hideDeclineButton()

        when(state) {
            Call.STATE_RINGING -> ringing(name, phoneNumber, avatar, accept, decline)
            Call.STATE_ACTIVE -> active(name, phoneNumber, avatar, accept, decline)
            Call.STATE_DISCONNECTED -> mView?.disconnect(
                !reject && !active && callHelper.missingCall, name, phoneNumber, 0
            )
            Call.STATE_CONNECTING, Call.STATE_DIALING -> dialing(name, phoneNumber, avatar, accept, decline)
        }

    }

    override fun detach() {
        mView = null
    }
}