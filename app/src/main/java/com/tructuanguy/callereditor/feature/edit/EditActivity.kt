package com.tructuanguy.callereditor.feature.edit

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.extensions.launchActivity
import com.tructuanguy.callereditor.databinding.ActivityEditBinding
import com.tructuanguy.callereditor.extensions.animateRightToLeft
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.isDefaultDialer
import com.tructuanguy.callereditor.extensions.setDefaultDialerIntent
import com.tructuanguy.callereditor.feature.audio.AudioActivity
import com.tructuanguy.callereditor.feature.background.BackgroundActivity
import com.tructuanguy.callereditor.feature.icon_avatar.IconAndAvatarActivity
import com.tructuanguy.callereditor.feature.preview.PreviewActivity
import com.tructuanguy.callereditor.feature.saved.SavedActivity
import com.tructuanguy.callereditor.feature.setting.SettingActivity
import com.tructuanguy.callereditor.helper.ImageHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val PREVIEW_BACKGROUND = "preview_bg"
const val PREVIEW_ACCEPT = "preview_acp"
const val PREVIEW_DECLINE = "preview_dec"
const val PREVIEW_AVATAR = "preview_avt"
const val PREVIEW_AUDIO = "preview_audio"
const val AUDIO_NAME = "audio_name"

@AndroidEntryPoint
class EditActivity : BaseActivity<ActivityEditBinding>(), EditContract.View {

    @Inject
    lateinit var mPresenter: EditContract.Presenter

    private var _backgroundPath = ""
    private var _acceptIconPath = ""
    private var _declineIconPath = ""
    private var _avatarPath = ""
    private var _audioPath = ""
    private var _audioName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        if (mPresenter.isEmptyListAvatar()) {
            mPresenter.insertAvatarApp()
        }
        mPresenter.getCurrentBackground()
        mPresenter.getCurrentAvatar()
        mPresenter.getCurrentAccept()
        mPresenter.getCurrentDecline()
        mPresenter.getCurrentAudioName()
        mPresenter.getCurrentAudioPath()
    }

    override fun initViews() {

        binding.apply {

            tvAudioName.animateRightToLeft()

            layoutSelectBackground.setOnClickListener {
                editBackground.launch(Intent(this@EditActivity, BackgroundActivity::class.java))
            }
            setting.setOnClickListener {
                launchActivity<SettingActivity> { }
            }
            iconAvatar.setOnClickListener {
                editIconAndAvatar.launch(
                    Intent(
                        this@EditActivity,
                        IconAndAvatarActivity::class.java
                    )
                )
            }
            audio.setOnClickListener {
                addAudio.launch(Intent(this@EditActivity, AudioActivity::class.java))
            }
            layoutSaved.setOnClickListener {
                launchActivity<SavedActivity> { }
            }

            cvPreview.setOnClickListener {
                launchActivity<PreviewActivity> {
                    putExtra(PREVIEW_BACKGROUND, _backgroundPath)
                    putExtra(PREVIEW_AVATAR, _avatarPath)
                    putExtra(PREVIEW_ACCEPT, _acceptIconPath)
                    putExtra(PREVIEW_DECLINE, _declineIconPath)
                    putExtra(PREVIEW_AUDIO, _audioPath)
                    putExtra(AUDIO_NAME, _audioName)
                }
            }
        }
    }

    private val addAudio = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        mPresenter.handleAudioResult(result.resultCode, result.data)
    }

    private val editBackground = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        mPresenter.handleBackgroundResult(result.resultCode, result.data)
    }

    private val editIconAndAvatar = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        mPresenter.handleIconAndAvatarResult(result.resultCode, result.data)
    }

    override fun getLayout(): ActivityEditBinding {
        return ActivityEditBinding.inflate(layoutInflater)
    }

    override fun showError(error: String) {
        Log.i("error_insert_app_avatar", error)
    }

    override fun displayAssetBackground(path: String) {
        _backgroundPath = path
        ImageHelper(this).loadBitmap(getBitmapFromAsset(path), binding.ivBackgroundCallerTheme)
    }

    override fun displayRemoteBackground(path: String) {
        _backgroundPath = path
        ImageHelper(this).loadUrl(path, binding.ivBackgroundCallerTheme)
    }

    override fun displayLocalBackground(path: String) {
        _backgroundPath = path
        ImageHelper(this).loadUri(path, binding.ivBackgroundCallerTheme)
    }

    override fun displayAssetAvatar(path: String) {
        _avatarPath = path
        ImageHelper(this).loadBitmap(getBitmapFromAsset(AVATAR_ASSET_PATH, path), binding.ivAvatar)
    }

    override fun displayLocalAvatar(path: String) {
        _avatarPath = path
        ImageHelper(this).loadUri(path, binding.ivAvatar)
    }

    override fun displayAccept(path: String) {
        _acceptIconPath = path
        ImageHelper(this).loadBitmap(getBitmapFromAsset(ACCEPT_ASSET_PATH, path), binding.ivAccept)
    }

    override fun displayDecline(path: String) {
        _declineIconPath = path
        ImageHelper(this).loadBitmap(
            getBitmapFromAsset(DECLINE_ASSET_PATH, path),
            binding.ivDecline
        )
    }

    override fun displayAudioName(name: String) {
        _audioName = name
        binding.tvAudioName.text = name
    }

    override fun getAudioPath(path: String) {
        _audioPath = path
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

    override fun onResume() {
        super.onResume()
        binding.tvAudioName.animateRightToLeft()
        if(!isDefaultDialer()) setDefaultDialerIntent()
    }

}