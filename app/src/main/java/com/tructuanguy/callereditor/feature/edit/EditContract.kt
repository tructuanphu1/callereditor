package com.tructuanguy.callereditor.feature.edit

import android.content.Intent
import com.tructuanguy.callereditor.core.ActivityContract

interface EditContract {
    interface View : ActivityContract.View {
        fun showError(error: String)
        fun displayAssetBackground(path: String)
        fun displayRemoteBackground(path: String)
        fun displayLocalBackground(path: String)
        fun displayAssetAvatar(path: String)
        fun displayLocalAvatar(path: String)
        fun displayAccept(path: String)
        fun displayDecline(path: String)
        fun displayAudioName(name: String)
        fun getAudioPath(path: String)
    }

    interface Presenter : ActivityContract.Presenter<View> {
        fun handleBackgroundResult(resultCode: Int, data: Intent?)
        fun handleIconAndAvatarResult(resultCode: Int, data: Intent?)
        fun handleAudioResult(resultCode: Int, data: Intent?)
        fun insertAvatarApp()
        fun isEmptyListAvatar(): Boolean
        fun saveDefaultScreen(background: String, avatar: String, accept: String, decline: String)
        fun getCurrentBackground()
        fun getCurrentAvatar()
        fun getCurrentAccept()
        fun getCurrentDecline()
        fun getCurrentAudioName()
        fun getCurrentAudioPath()
    }
}