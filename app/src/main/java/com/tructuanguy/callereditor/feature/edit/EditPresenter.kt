package com.tructuanguy.callereditor.feature.edit

import android.content.Intent
import android.content.res.AssetManager
import android.util.Log
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_ACCEPT
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_NAME
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_AVATAR
import com.tructuanguy.callereditor.core.Constants.DIALER_BACKGROUND
import com.tructuanguy.callereditor.core.Constants.DIALER_DECLINE
import com.tructuanguy.callereditor.core.Constants.URI_SIGNAL
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.feature.audio.CHOSEN_AUDIO
import com.tructuanguy.callereditor.feature.background.CHOOSE_LOCAL_BACKGROUND
import com.tructuanguy.callereditor.feature.background.CHOOSE_REMOTE_BACKGROUND
import com.tructuanguy.callereditor.feature.icon_avatar.CHOOSE_ICON_AND_AVATAR_CODE
import com.tructuanguy.callereditor.helper.PreferencesHelper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class EditPresenter @Inject constructor(
    private val callerDB: CallerDatabase,
    private val prefs: PreferencesHelper
) : EditContract.Presenter {

    @Inject
    lateinit var mAssetManager: AssetManager

    private var mView: EditContract.View? = null

    override fun handleBackgroundResult(resultCode: Int, data: Intent?) {
        when (resultCode) {
            CHOOSE_REMOTE_BACKGROUND -> {
                val path: String = data?.extras?.getString(Constants.CHOOSE_REMOTE_BACKGROUND) as String
                mView?.displayRemoteBackground(path)
            }

            CHOOSE_LOCAL_BACKGROUND -> {
                val path: String = data?.extras?.getString(Constants.CHOOSE_LOCAL_BACKGROUND) as String
                mView?.displayLocalBackground(path)
            }
        }
    }

    override fun handleIconAndAvatarResult(resultCode: Int, data: Intent?) {
        when(resultCode) {
            CHOOSE_ICON_AND_AVATAR_CODE -> {
                val avatarPath = data?.extras?.getString(Constants.CHOOSE_AVATAR) as String
                val acceptIconPath = data.extras?.getString(Constants.CHOOSE_ACCEPT_ICON) as String
                val declineIconPath = data.extras?.getString(Constants.CHOOSE_DECLINE_ICON) as String
                if(avatarPath.isNotEmpty()) {
                    if (avatarPath.contains(APP_PATH)) mView?.displayLocalAvatar(avatarPath) else mView?.displayAssetAvatar(avatarPath)
                }
                if(acceptIconPath.isNotEmpty()) mView?.displayAccept(acceptIconPath)
                if(declineIconPath.isNotEmpty()) mView?.displayDecline(declineIconPath)
            }
        }
    }

    override fun handleAudioResult(resultCode: Int, data: Intent?) {
        when(resultCode) {
            CHOSEN_AUDIO -> {
                val path = data?.extras?.getString(Constants.CHOSEN_AUDIO_PATH) as String
                val name = data.extras?.getString(Constants.CHOSEN_AUDIO_NAME) as String
                mView?.displayAudioName(name)
                mView?.getAudioPath(path)
            }
        }
    }

    override fun insertAvatarApp() {
        val listAvatar = getAssetAvatar()
        for (i in listAvatar.indices) {
            callerDB.getCallerDao().insertAvatarDevice(Avatar(0, listAvatar[i]))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {
                        // do nothing
                    }

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {

                    }
                })
        }
    }

    override fun isEmptyListAvatar(): Boolean {
        try {
            val list = callerDB.getCallerDao().getAllAvatarDevice()
            return list.isEmpty()
        } catch (e: Exception) {
            mView?.showError(e.toString())
        }
        return false
    }

    override fun saveDefaultScreen(
        background: String,
        avatar: String,
        accept: String,
        decline: String
    ) {
        if(prefs.getString(DIALER_BACKGROUND)!!.isEmpty()) {
            prefs.saveString(DIALER_BACKGROUND, background)
            prefs.saveString(DIALER_AVATAR, avatar)
            prefs.saveString(DIALER_ACCEPT, accept)
            prefs.saveString(DIALER_DECLINE, decline)
            prefs.saveString(DIALER_AUDIO_PATH, "")
            prefs.saveString(DIALER_AUDIO_NAME, "")
        }
    }

    override fun getCurrentBackground() {
        val background = prefs.getString(DIALER_BACKGROUND).toString()
        if(background.contains(URI_SIGNAL)) {
            mView?.displayLocalBackground(background)
        } else if(background.contains(APP_PATH)) {
            mView?.displayLocalBackground(path = background)
        } else {
            mView?.displayAssetBackground(path = background)
        }
    }

    override fun getCurrentAvatar() {
        val avatar = prefs.getString(DIALER_AVATAR).toString()
        if(avatar.contains(APP_PATH)) {
            mView?.displayLocalAvatar(path = avatar)
        } else {
            mView?.displayAssetAvatar(avatar)
        }
    }

    override fun getCurrentAccept() {
        mView?.displayAccept(prefs.getString(DIALER_ACCEPT).toString())
    }

    override fun getCurrentDecline() {
        mView?.displayDecline(prefs.getString(DIALER_DECLINE).toString())
    }

    override fun getCurrentAudioName() {
        val name = prefs.getString(DIALER_AUDIO_NAME).toString()
        if(name.isNotEmpty()) {
            mView?.displayAudioName(name)
        }
    }

    override fun getCurrentAudioPath() {
        val path = prefs.getString(DIALER_AUDIO_PATH).toString()
        if(path.isNotEmpty()) {
            mView?.getAudioPath(path)
        }
    }

    private fun getAssetAvatar(): MutableList<String> {
        val assetList = mutableListOf<String>()
        val files = mAssetManager.list(AVATAR_ASSET_PATH)
        if (files!!.isNotEmpty()) {
            for (file in files) {
                assetList.add(file)
            }
        }
        return assetList
    }


    override fun attach(view: EditContract.View) {
        this.mView = view
    }

    override fun detach() {
        mView = null
    }
}