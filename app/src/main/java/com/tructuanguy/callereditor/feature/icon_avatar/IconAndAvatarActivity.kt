package com.tructuanguy.callereditor.feature.icon_avatar

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.BaseOptionDialog
import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.core.OnItemClickListener
import com.tructuanguy.callereditor.core.OptionDialogListener
import com.tructuanguy.callereditor.databinding.ActivityIconAvatarBinding
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.extensions.getLaunchGalleryIntent
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.feature.icon_avatar.adapter.AvatarAdapter
import com.tructuanguy.callereditor.feature.icon_avatar.adapter.IconAdapter
import com.tructuanguy.callereditor.feature.icon_avatar.model.CallerIcon
import com.tructuanguy.callereditor.helper.PermissionHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val CHOOSE_ICON_AND_AVATAR_CODE = 2610

@AndroidEntryPoint
class IconAndAvatarActivity : BaseActivity<ActivityIconAvatarBinding>(), IconAndAvatarContract.View {

    @Inject
    lateinit var mPresenter: IconAndAvatarPresenter

    @Inject
    lateinit var mPermissionHelper: PermissionHelper

    private var chosenAvatarPath = ""
    private var chosenDeclinePath = ""
    private var chosenAcceptPath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.applyCallerIcon()
        mPresenter.applyAvatar()
    }

    override fun initViews() {
        binding.apply {
            addAvatar.setOnClickListener {
                if(mPermissionHelper.isStoragePermissionGranted()) {
                    pickImage.launch(getLaunchGalleryIntent())
                } else {
                    requestPermissions(arrayOf(
                        if(mPermissionHelper.isAndroid13()) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), Constants.REQUEST_PERMISSION_CODE)
                }
            }
            save.setOnClickListener {
                val intent = Intent()
                intent.apply {
                    putExtra(Constants.CHOOSE_AVATAR, chosenAvatarPath)
                    putExtra(Constants.CHOOSE_ACCEPT_ICON, chosenAcceptPath)
                    putExtra(Constants.CHOOSE_DECLINE_ICON, chosenDeclinePath)
                }
                setResult(CHOOSE_ICON_AND_AVATAR_CODE, intent)
                finish()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Constants.REQUEST_PERMISSION_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage.launch(getLaunchGalleryIntent())
        } else {
            showRequestDetailSettingsDialog()
        }
    }

    private val pickImage = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if(result.resultCode == RESULT_OK) {
            val uri = result.data!!.data
            val bitmap = mPresenter.getBitmapFromUri(uri.toString())
            mPresenter.saveBitmap(bitmap)
            Log.i("666", mPresenter.getBitmapPath(bitmap))
            mPresenter.insertDeviceAvatar(Avatar(0, mPresenter.getBitmapPath(bitmap)))
        }
    }

    override fun getLayout(): ActivityIconAvatarBinding {
        return ActivityIconAvatarBinding.inflate(layoutInflater)
    }


    override fun updateCallerIconAdapter(list: MutableList<CallerIcon>) {
        binding.rcvIcon.adapter = IconAdapter(list, object : OnItemClickListener {
            override fun onItemClick(data: Any) {
                val icon = data as CallerIcon
                chosenAcceptPath = icon.accept
                chosenDeclinePath = icon.decline
            }
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun updateAvatarAdapter(list: MutableList<Avatar>) {
        lateinit var adapter: AvatarAdapter
        val handleClick: (Avatar, Int) -> Unit = { avatar, _ ->
            chosenAvatarPath = avatar.path
        }
        val handleLongClick: (Avatar, Int) -> Unit = { avatar, pos ->
            run {
                val dialog = BaseOptionDialog(this@IconAndAvatarActivity)
                dialog.apply {
                    setOptionTitleValue(getString(R.string.delete_saved_avatar_device_title))
                    setOptionMessageValue(getString(R.string.delete_saved_image_device_message))
                    setOptionNegativeValue(getString(R.string.cancel))
                    setOptionPositiveValue(getString(R.string.delete))
                }
                dialog.setOptionListener(object : OptionDialogListener {
                    override fun onNegative() {
                        // do nothing
                    }

                    override fun onPositive() {
                        mPresenter.deleteDeviceAvatar(avatar)
                        adapter.notifyItemRemoved(pos)
                    }
                })
                dialog.show()
            }
        }
        adapter = AvatarAdapter(list, clickListener = handleClick, longClickListener = handleLongClick)
        adapter.submitList(list)
        Log.i("333", list.toString())
        runOnUiThread {
            binding.rcvAvatar.adapter = adapter
            adapter.notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onInsertAvatarSuccess() {
        shortToast(getString(R.string.insert_successfully))
        mPresenter.applyAvatar()
    }

    override fun onDeleteAvatarSuccess() {
        shortToast(getString(R.string.delete_successfully))
        mPresenter.applyAvatar()
    }

    override fun showError(error: String) {
        Log.i("icon_avatar_error", error)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}