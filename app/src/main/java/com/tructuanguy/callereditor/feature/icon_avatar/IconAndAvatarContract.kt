package com.tructuanguy.callereditor.feature.icon_avatar

import android.graphics.Bitmap
import com.tructuanguy.callereditor.core.ActivityContract
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.feature.icon_avatar.model.CallerIcon

interface IconAndAvatarContract {

    interface View : ActivityContract.View {
        fun updateCallerIconAdapter(list: MutableList<CallerIcon>)
        fun updateAvatarAdapter(list: MutableList<Avatar>)
        fun onInsertAvatarSuccess()
        fun onDeleteAvatarSuccess()
        fun showError(error: String)
    }

    interface Presenter : ActivityContract.Presenter<View> {
        fun getAssetAvatar(): MutableList<String>
        fun applyCallerIcon()
        fun applyAvatar()
        fun getAssetAcceptIcon(): MutableList<String>
        fun getAssetDeclineIcon(): MutableList<String>
        fun insertDeviceAvatar(avatar: Avatar)
        fun deleteDeviceAvatar(avatar: Avatar)
        fun getAllAvatar(): List<Avatar>
        fun getBitmapFromUri(uriPath: String): Bitmap
        fun saveBitmap(bitmap: Bitmap)
        fun getBitmapPath(bitmap: Bitmap): String
    }

}