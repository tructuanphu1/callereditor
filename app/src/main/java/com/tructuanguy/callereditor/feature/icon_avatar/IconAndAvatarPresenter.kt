package com.tructuanguy.callereditor.feature.icon_avatar

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.extensions.saveBitmap
import com.tructuanguy.callereditor.feature.icon_avatar.model.CallerIcon
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class IconAndAvatarPresenter @Inject constructor(
    private val context: Context,
    private val callerDB: CallerDatabase
) : IconAndAvatarContract.Presenter, CoroutineScope {

    @Inject
    lateinit var mAssetManager: AssetManager

    private var mView: IconAndAvatarContract.View? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun getAssetAvatar(): MutableList<String> {
        val assetList = mutableListOf<String>()
        val files = mAssetManager.list(AVATAR_ASSET_PATH)
        if(files!!.isNotEmpty()) {
            for(file in files) {
                assetList.add(file)
            }
        }
        return assetList
    }



    override fun applyCallerIcon() {
        val listAccept = getAssetAcceptIcon()
        val listDecline = getAssetDeclineIcon()
        val listCallerIcon = mutableListOf<CallerIcon>()
        for(i in listAccept.indices) {
            listCallerIcon.add(CallerIcon(listDecline[i], listAccept[i]))
        }
        launch {
            withContext(Dispatchers.IO) {
                mView?.updateCallerIconAdapter(listCallerIcon)
            }
        }
    }

    override fun applyAvatar() {
        launch {
            withContext(Dispatchers.IO) {
                mView?.updateAvatarAdapter(getAllAvatar().toMutableList())
            }
        }
    }

    override fun getAssetAcceptIcon(): MutableList<String> {
        val assetList = mutableListOf<String>()
        val files = mAssetManager.list(ACCEPT_ASSET_PATH)
        if(files!!.isNotEmpty()) {
            for(file in files) {
                assetList.add(file)
            }
        }
        return assetList
    }

    override fun getAssetDeclineIcon(): MutableList<String> {
        val assetList = mutableListOf<String>()
        val files = mAssetManager.list(DECLINE_ASSET_PATH)
        if(files!!.isNotEmpty()) {
            for(file in files) {
                assetList.add(file)
            }
        }
        return assetList
    }

    override fun insertDeviceAvatar(avatar: Avatar) {
        callerDB.getCallerDao().insertAvatarDevice(avatar)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    // do nothing
                }

                override fun onComplete() {
                    mView?.onInsertAvatarSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }

    override fun deleteDeviceAvatar(avatar: Avatar) {
        callerDB.getCallerDao().deleteAvatarDevice(avatar.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    // do nothing
                }

                override fun onComplete() {
                    mView?.onDeleteAvatarSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }

    override fun getAllAvatar(): List<Avatar> {
        try {
            return callerDB.getCallerDao().getAllAvatarDevice()
        } catch (e: Exception) {
            mView?.showError(e.toString())
        }
        return emptyList()
    }

    @Suppress("DEPRECATION")
    override fun getBitmapFromUri(uriPath: String): Bitmap {
        return if(Build.VERSION.SDK_INT >= 29) {
            val source = ImageDecoder.createSource(context.contentResolver, Uri.parse(uriPath))
            ImageDecoder.decodeBitmap(source)
        } else {
            MediaStore.Images.Media.getBitmap(context.contentResolver, Uri.parse(uriPath))
        }
    }

    override fun saveBitmap(bitmap: Bitmap) {
        context.saveBitmap(bitmap)
    }

    override fun getBitmapPath(bitmap: Bitmap): String {
        return context.saveBitmap(bitmap)
    }


    override fun attach(view: IconAndAvatarContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }


}