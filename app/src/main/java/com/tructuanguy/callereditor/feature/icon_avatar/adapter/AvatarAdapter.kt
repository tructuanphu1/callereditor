package com.tructuanguy.callereditor.feature.icon_avatar.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.URI_SIGNAL
import com.tructuanguy.callereditor.databinding.ItemAvatarAppBinding
import com.tructuanguy.callereditor.db.entities.Avatar
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.helper.ImageHelper

class AvatarAdapter(
    private val listAvatarApp: MutableList<Avatar>,
    private val clickListener: (Avatar, Int) -> Unit,
    private val longClickListener: (Avatar, Int) -> Unit
) : ListAdapter<Avatar, AvatarAdapter.AvatarViewHolder>(AvatarDiffCallback()) {

    private var selectedPosition = RecyclerView.NO_POSITION
    private var isChosen = false

    inner class AvatarViewHolder(private val binding: ItemAvatarAppBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(avatar: Avatar) {
            if (avatar.path.contains(APP_PATH)) {
                ImageHelper(itemView.context).loadUri(avatar.path, binding.ivAvatar)
                itemView.setOnLongClickListener {
                    longClickListener.invoke(avatar, adapterPosition)
                    true
                }
            } else {
                ImageHelper(itemView.context).loadBitmap(
                    itemView.context.getBitmapFromAsset(
                        "Images/avatar",
                        avatar.path
                    ), binding.ivAvatar
                )
            }
            itemView.setOnClickListener {
                setChosen(adapterPosition)
                isChosen = true
                clickListener.invoke(avatar, adapterPosition)
            }
            if (adapterPosition == selectedPosition) binding.ivChosen.visible() else binding.ivChosen.gone()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AvatarViewHolder {
        return AvatarViewHolder(
            ItemAvatarAppBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = listAvatarApp.size
    override fun onBindViewHolder(holder: AvatarViewHolder, position: Int) {
        holder.bind(listAvatarApp[position])
    }

    private fun setChosen(position: Int) {
        val previousSelectedPosition = selectedPosition
        selectedPosition = position
        for (index in 0 until listAvatarApp.size) {
            isChosen = (index == position)
        }
        notifyItemChanged(previousSelectedPosition)
        notifyItemChanged(selectedPosition)
    }

}

class AvatarDiffCallback: DiffUtil.ItemCallback<Avatar>() {
    override fun areItemsTheSame(oldItem: Avatar, newItem: Avatar): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Avatar, newItem: Avatar): Boolean {
        return oldItem.path == newItem.path
    }

}