package com.tructuanguy.callereditor.feature.icon_avatar.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.core.OnItemClickListener
import com.tructuanguy.callereditor.databinding.ItemCallerIconBinding
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.feature.icon_avatar.model.CallerIcon
import com.tructuanguy.callereditor.helper.ImageHelper

class IconAdapter(
    private val listIcon: List<CallerIcon>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<IconAdapter.IconViewHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION
    private var isChosen = false

    inner class IconViewHolder(private val binding: ItemCallerIconBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(icon: CallerIcon) {
            binding.apply {
                ImageHelper(itemView.context).loadBitmap(itemView.context.getBitmapFromAsset("Images/accept", icon.accept), ivAccept)
                ImageHelper(itemView.context).loadBitmap(itemView.context.getBitmapFromAsset("Images/decline", icon.decline), ivDecline)
            }
            itemView.setOnClickListener {
                setChosen(adapterPosition)
                isChosen = true
                listener.onItemClick(icon)
            }
            if(adapterPosition == selectedPosition) binding.ivChosen.visible() else binding.ivChosen.gone()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IconViewHolder {
        return IconViewHolder(ItemCallerIconBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return listIcon.size
    }

    override fun onBindViewHolder(holder: IconViewHolder, position: Int) {
        holder.bind(listIcon[position])
    }

    private fun setChosen(position: Int) {
        val previousSelectedPosition = selectedPosition
        selectedPosition = position
        for (index in listIcon.indices) {
            val color = listIcon[index]
            isChosen = (index == position)
        }
        notifyItemChanged(previousSelectedPosition)
        notifyItemChanged(selectedPosition)
    }

}