package com.tructuanguy.callereditor.feature.icon_avatar.model

import java.io.Serializable

data class CallerIcon(
    val decline: String,
    val accept: String
): Serializable
