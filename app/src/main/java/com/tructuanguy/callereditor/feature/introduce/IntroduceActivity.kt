package com.tructuanguy.callereditor.feature.introduce

import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.databinding.ActivityIntroduceBinding
import com.tructuanguy.callereditor.feature.introduce.onboarding.IntroOneFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class IntroduceActivity : BaseActivity<ActivityIntroduceBinding>() {

    override fun initViews() {
        val fragmentIntroOne = IntroOneFragment()
        val trans = supportFragmentManager.beginTransaction()
        trans.apply {
            replace(R.id.fr_introduce, fragmentIntroOne)
            commit()
        }
    }

    override fun getLayout(): ActivityIntroduceBinding =
        ActivityIntroduceBinding.inflate(layoutInflater)
}