package com.tructuanguy.callereditor.feature.introduce

import com.tructuanguy.callereditor.core.ActivityContract

interface IntroduceContract {

    interface View: ActivityContract.View {

    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun saveDefaultScreen()
        fun saveFirstInAppFlag(flag: Boolean)
    }

}