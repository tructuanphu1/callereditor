package com.tructuanguy.callereditor.feature.introduce

import com.tructuanguy.callereditor.core.Constants
import com.tructuanguy.callereditor.core.Constants.DIALER_ACCEPT
import com.tructuanguy.callereditor.core.Constants.DIALER_AVATAR
import com.tructuanguy.callereditor.core.Constants.DIALER_BACKGROUND
import com.tructuanguy.callereditor.core.Constants.DIALER_DECLINE
import com.tructuanguy.callereditor.core.Constants.IS_FIRST_IN_APP
import com.tructuanguy.callereditor.helper.PreferencesHelper
import javax.inject.Inject

class IntroducePresenter @Inject constructor(
    private val prefs: PreferencesHelper
) : IntroduceContract.Presenter{

    private var mView: IntroduceContract.View? = null
    override fun saveDefaultScreen() {
        prefs.saveString(DIALER_BACKGROUND, "${Constants.BACKGROUND_ASSET_PATH}/background.jpg")
        prefs.saveString(DIALER_AVATAR, "av0.png")
        prefs.saveString(DIALER_ACCEPT, "ic_ac1.png")
        prefs.saveString(DIALER_DECLINE, "ic_dc1.png")
    }

    override fun saveFirstInAppFlag(flag: Boolean) {
        prefs.saveBoolean(IS_FIRST_IN_APP, flag)
    }


    override fun attach(view: IntroduceContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}