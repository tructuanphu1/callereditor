package com.tructuanguy.callereditor.feature.introduce.onboarding

import android.view.View
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.BaseFragment
import com.tructuanguy.callereditor.databinding.FragmentIntro1Binding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroOneFragment : BaseFragment<FragmentIntro1Binding>() {
    override fun initView(view: View) {
        binding.cvContinue.setOnClickListener {
            val trans = requireActivity().supportFragmentManager.beginTransaction()
            trans.apply {
                setCustomAnimations(
                    R.anim.slide_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.slide_out
                )
                replace(R.id.fr_introduce, IntroTwoFragment())
                commit()
            }
        }
    }

    override fun getViewBinding(): FragmentIntro1Binding =
        FragmentIntro1Binding.inflate(layoutInflater)
}