package com.tructuanguy.callereditor.feature.introduce.onboarding

import android.os.Bundle
import android.view.View
import com.tructuanguy.callereditor.feature.edit.EditActivity
import com.tructuanguy.callereditor.core.BaseFragment
import com.tructuanguy.callereditor.databinding.FragmentIntro3Binding
import com.tructuanguy.callereditor.extensions.launchActivity
import com.tructuanguy.callereditor.feature.introduce.IntroduceContract
import com.tructuanguy.callereditor.feature.introduce.IntroducePresenter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class IntroThreeFragment : BaseFragment<FragmentIntro3Binding>(), IntroduceContract.View {

    @Inject
    lateinit var mPresenter: IntroducePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
    }


    override fun initView(view: View) {

        binding.cvStart.setOnClickListener {
            mPresenter.saveDefaultScreen()
            mPresenter.saveFirstInAppFlag(false)
            requireActivity().launchActivity<EditActivity> {  }
            requireActivity().finish()
        }
    }

    override fun getViewBinding(): FragmentIntro3Binding =
        FragmentIntro3Binding.inflate(layoutInflater)

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}