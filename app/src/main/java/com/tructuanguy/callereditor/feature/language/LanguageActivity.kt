package com.tructuanguy.callereditor.feature.language

import android.os.Bundle
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.databinding.ActivityLanguageBinding
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.feature.language.model.Language
import com.tructuanguy.callereditor.feature.language.adapter.LanguageAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LanguageActivity : BaseActivity<ActivityLanguageBinding>(), LanguageContract.View {

    @Inject
    lateinit var mPresenter: LanguagePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.applyLanguage()
    }

    override fun initViews() {

    }

    override fun getLayout(): ActivityLanguageBinding {
        return ActivityLanguageBinding.inflate(layoutInflater)
    }

    override fun updateAdapter(list: ArrayList<Language>) {
        val handleClick: (Language) -> Unit = { language ->
            shortToast(language.name)
        }
        val adapter = LanguageAdapter(list, handleClick)
        binding.rcvLanguage.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }
}