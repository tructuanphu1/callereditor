package com.tructuanguy.callereditor.feature.language

import com.tructuanguy.callereditor.core.ActivityContract
import com.tructuanguy.callereditor.feature.language.model.Language

interface LanguageContract {

    interface View: ActivityContract.View {
        fun updateAdapter(list: ArrayList<Language>)
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun getLanguageList(): ArrayList<Language>
        fun applyLanguage()
    }

}