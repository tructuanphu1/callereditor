package com.tructuanguy.callereditor.feature.language

import android.content.Context
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.feature.language.model.Language
import javax.inject.Inject

class LanguagePresenter @Inject constructor(
    private val context: Context
): LanguageContract.Presenter {

    private var mView: LanguageContract.View? = null

    override fun getLanguageList(): ArrayList<Language> {
        val list = arrayListOf<Language>()
        list.add(Language(R.drawable.english, context.getString(R.string.english)))
        list.add(Language(R.drawable.vietnam, context.getString(R.string.vietnamese)))
        return list
    }

    override fun applyLanguage() {
        val list = getLanguageList()
        mView?.updateAdapter(list)
    }

    override fun attach(view: LanguageContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}