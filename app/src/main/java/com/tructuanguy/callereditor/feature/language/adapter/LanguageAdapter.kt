package com.tructuanguy.callereditor.feature.language.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.databinding.ItemLanguageBinding
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.feature.language.model.Language

class LanguageAdapter(
    private val list: ArrayList<Language>,
    private val listener: (Language) -> Unit
) : RecyclerView.Adapter<LanguageAdapter.LanguageHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION
    private var isChosen = false

    inner class LanguageHolder(private val binding: ItemLanguageBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(language: Language) {
            binding.apply {
                flag.setImageResource(language.flag)
                name.text = language.name
            }
            itemView.setOnClickListener {
                setChosen(adapterPosition)
                isChosen = true
                listener.invoke(language)
            }
            if (adapterPosition == selectedPosition) binding.chosen.visible() else binding.chosen.gone()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageHolder {
        return LanguageHolder(ItemLanguageBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }



    override fun onBindViewHolder(holder: LanguageHolder, position: Int) {
        holder.bind(list[position])
    }

    private fun setChosen(position: Int) {
        val previousSelectedPosition = selectedPosition
        selectedPosition = position
        for (index in list.indices) {
            isChosen = (index == position)
        }
        notifyItemChanged(previousSelectedPosition)
        notifyItemChanged(selectedPosition)
    }

}