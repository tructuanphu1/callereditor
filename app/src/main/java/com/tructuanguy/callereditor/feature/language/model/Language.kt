package com.tructuanguy.callereditor.feature.language.model

data class Language(
    val flag: Int,
    val name: String
)
