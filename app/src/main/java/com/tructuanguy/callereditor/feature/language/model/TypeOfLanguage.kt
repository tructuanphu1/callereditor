package com.tructuanguy.callereditor.feature.language.model

enum class TypeOfLanguage {
    URDU,
    RUSSIA,
    PORTUGAL,
    BENGAL,
    ARAB,
    FRENCH,
    SPANISH,
    HINDI,
    ENGLISH,
    VIETNAMESE
}