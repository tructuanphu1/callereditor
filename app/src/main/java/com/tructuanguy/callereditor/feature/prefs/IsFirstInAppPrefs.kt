package com.tructuanguy.callereditor.feature.prefs

import android.content.Context
import com.tructuanguy.callereditor.core.Constants.IS_FIRST_IN_APP

class IsFirstInAppPrefs(context: Context) {


    private val prefs = context.getSharedPreferences(IS_FIRST_IN_APP, Context.MODE_PRIVATE)

    fun saveFirstIn(isFirstIn: Boolean) {
        prefs.edit().putBoolean(IS_FIRST_IN_APP, isFirstIn).apply()
    }

    fun isFirstInApp(): Boolean {
        return prefs.getBoolean(IS_FIRST_IN_APP, false)
    }
}