package com.tructuanguy.callereditor.feature.preview

import android.graphics.Bitmap
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.URL_PATH
import com.tructuanguy.callereditor.databinding.ActivityPreviewBinding
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.launchActivity
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.feature.edit.AUDIO_NAME
import com.tructuanguy.callereditor.feature.edit.PREVIEW_ACCEPT
import com.tructuanguy.callereditor.feature.edit.PREVIEW_AUDIO
import com.tructuanguy.callereditor.feature.edit.PREVIEW_AVATAR
import com.tructuanguy.callereditor.feature.edit.PREVIEW_BACKGROUND
import com.tructuanguy.callereditor.feature.edit.PREVIEW_DECLINE
import com.tructuanguy.callereditor.feature.saved.SavedActivity
import com.tructuanguy.callereditor.helper.ImageHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PreviewActivity : BaseActivity<ActivityPreviewBinding>(), PreviewContract.View {

    @Inject
    lateinit var mPresenter: PreviewPresenter
    private var mediaPlayer: MediaPlayer? = null
    private var imageUriPathFromUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.applyPreviewScreen()
    }

    override fun initViews() {
        binding.icBack.setOnClickListener {
            finish()
        }

        binding.cvApply.setOnClickListener {
            if (getBackgroundPath().contains(URL_PATH)) {
                if (!mPresenter.isFileExist(getBackgroundPath())) {
                    mPresenter.downloadImageUrl(getBackgroundPath())
                } else {
                    imageUriPathFromUrl = mPresenter.getImageFileNameDownloaded(getBackgroundPath())
                    mPresenter.saveApplyDialerScreenWithBackgroundUriFromUrl()
                    mPresenter.insertDialerScreen(
                        DialerScreen(
                            0,
                            mPresenter.getImageFileNameDownloaded(getBackgroundPath()),
                            getAvatarPath(),
                            getAcceptPath(),
                            getDeclinePath(),
                            getAudioPath(),
                            getAudioName()
                        )
                    )
                }
            } else {
                mPresenter.saveApplyDialerScreen()
                mPresenter.insertDialerScreen(
                    DialerScreen(
                        0,
                        getBackgroundPath(),
                        getAvatarPath(),
                        getAcceptPath(),
                        getDeclinePath(),
                        getAudioPath(),
                        getAudioName()
                    )
                )
            }
        }
    }

    private fun navigateToSavedScreen() {
        Handler(mainLooper).postDelayed({
            launchActivity<SavedActivity> {  }
            finish()
        }, 500)
    }

    override fun getLayout(): ActivityPreviewBinding {
        return ActivityPreviewBinding.inflate(layoutInflater)
    }

    override fun getBackgroundPathUriFromUrlNotDownload(): String {
        Log.i("nam", imageUriPathFromUrl)
        return imageUriPathFromUrl
    }

    override fun getBackgroundPath(): String {
        return intent.extras?.getString(PREVIEW_BACKGROUND).toString()
    }

    override fun getAvatarPath(): String {
        return intent.extras?.getString(PREVIEW_AVATAR).toString()
    }

    override fun getAcceptPath(): String {
        return intent.extras?.getString(PREVIEW_ACCEPT).toString()
    }

    override fun getDeclinePath(): String {
        return intent.extras?.getString(PREVIEW_DECLINE).toString()
    }

    override fun getAudioPath(): String {
        return intent.extras?.getString(PREVIEW_AUDIO).toString()
    }

    override fun getAudioName(): String {
        return intent.extras?.getString(AUDIO_NAME).toString()
    }

    override fun displayBackgroundUriFromUrl(path: String) {
        ImageHelper(this).loadUrl(path, binding.ivBackground)
    }

    override fun displayBackgroundUri(path: String) {
        ImageHelper(this).loadUri(path, binding.ivBackground)
    }

    override fun displayBackgroundAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(path), binding.ivBackground)
    }

    override fun displayAvatarAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(AVATAR_ASSET_PATH, path), binding.ivAvatar)
    }

    override fun displayAvatarUri(path: String) {
        ImageHelper(this).loadUri(path, binding.ivAvatar)
    }

    override fun displayAccept(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(ACCEPT_ASSET_PATH, path), binding.accept)
    }

    override fun displayDecline(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(DECLINE_ASSET_PATH, path), binding.decline)
    }

    override fun displayAudio(path: String) {
        mediaPlayer = MediaPlayer()
        try {
            mediaPlayer?.setDataSource(path)
            mediaPlayer?.prepare()
        } catch (e: Exception) {
            Log.i("error", e.toString())
        }
        mediaPlayer?.start()
        Log.i("path_audio", path)
    }

    override fun notifyApplySuccess() {
        Handler(mainLooper).postDelayed({ shortToast(getString(R.string.apply_successfully)) }, 250)
    }

    override fun insertSuccess() {
        shortToast(getString(R.string.insert_successfully))
        navigateToSavedScreen()
    }

    override fun showError(error: String) {
        Log.i("error", error)
    }

    override fun onDownloadSuccess(name: String) {
        imageUriPathFromUrl = name
        mPresenter.saveApplyDialerScreenWithBackgroundUriFromUrl()
        mPresenter.insertDialerScreen(
            DialerScreen(
                0,
                name,
                getAvatarPath(),
                getAcceptPath(),
                getDeclinePath(),
                getAudioPath(),
                getAudioName()
            )
        )
    }

    override fun showLoading(isShow: Boolean) {
        if (isShow) showLoadingDialog() else hideLoadingDialog()
    }


    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
        if (mediaPlayer != null) {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = null
        }
    }


}