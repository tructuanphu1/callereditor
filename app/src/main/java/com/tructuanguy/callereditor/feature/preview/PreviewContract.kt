package com.tructuanguy.callereditor.feature.preview

import android.graphics.Bitmap
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.core.ActivityContract

interface PreviewContract {

    interface View: ActivityContract.View {
        fun getBackgroundPathUriFromUrlNotDownload(): String
        fun getBackgroundPath(): String
        fun getAvatarPath(): String
        fun getAcceptPath(): String
        fun getDeclinePath(): String
        fun getAudioPath(): String
        fun getAudioName(): String
        fun displayBackgroundUriFromUrl(path: String)
        fun displayBackgroundUri(path: String)
        fun displayBackgroundAsset(path: String)
        fun displayAvatarAsset(path: String)
        fun displayAvatarUri(path: String)
        fun displayAccept(path: String)
        fun displayDecline(path: String)
        fun displayAudio(path: String)
        fun notifyApplySuccess()
        fun insertSuccess()
        fun showError(error: String)
        fun onDownloadSuccess(name: String)
        fun showLoading(isShow: Boolean)
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun applyPreviewScreen()
        fun saveApplyDialerScreen()
        fun getImageFileNameDownloaded(url: String): String
        fun saveApplyDialerScreenWithBackgroundUriFromUrl()
        fun insertDialerScreen(screen: DialerScreen)
        fun isFileExist(path: String): Boolean
        fun downloadImageUrl(url: String)
        fun getBitmapFromUri(uriPath: String): Bitmap
        fun saveBitmap(bitmap: Bitmap)
        fun getBitmapPath(bitmap: Bitmap): String
    }

}