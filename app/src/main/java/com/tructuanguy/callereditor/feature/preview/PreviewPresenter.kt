package com.tructuanguy.callereditor.feature.preview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.core.Constants.DIALER_ACCEPT
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_NAME
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_AVATAR
import com.tructuanguy.callereditor.core.Constants.DIALER_BACKGROUND
import com.tructuanguy.callereditor.core.Constants.DIALER_DECLINE
import com.tructuanguy.callereditor.core.Constants.URI_SIGNAL
import com.tructuanguy.callereditor.core.Constants.URL_PATH
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.extensions.saveBitmap
import com.tructuanguy.callereditor.helper.FileHelper
import com.tructuanguy.callereditor.helper.PreferencesHelper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

class PreviewPresenter @Inject constructor(
    private val context: Context,
    private val mFileHelper: FileHelper,
    private val prefs: PreferencesHelper,
    private val callerDB: CallerDatabase
) : PreviewContract.Presenter {

    private var mView: PreviewContract.View? = null
    private var backgroundFileName = ""

    override fun applyPreviewScreen() {
        val background = mView?.getBackgroundPath()
        val avatar = mView?.getAvatarPath()
        val accept = mView?.getAcceptPath()
        val decline = mView?.getDeclinePath()
        val audioPath = mView?.getAudioPath()
        //val audioName = mView?.getAudioName()
        if(background!!.isNotEmpty()) {
            if (background.contains(APP_PATH)) {
                mView?.displayBackgroundUri(background)
            } else if(background.contains(URL_PATH)){
                mView?.displayBackgroundUriFromUrl(background)
            } else {
                mView?.displayBackgroundAsset(background)
            }
        }
        if(avatar!!.isNotEmpty()) {
            if (avatar.contains(APP_PATH)) {
                mView?.displayAvatarUri(avatar)
            } else {
                mView?.displayAvatarAsset(avatar)
            }
        }
        if(accept!!.isNotEmpty()) { mView?.displayAccept(accept) }
        if(decline!!.isNotEmpty()) { mView?.displayDecline(decline) }
        if(audioPath!!.isNotEmpty()) { mView?.displayAudio(audioPath) }
    }

    override fun saveApplyDialerScreen() {
        val background = mView?.getBackgroundPath()
        val avatar = mView?.getAvatarPath()
        val accept = mView?.getAcceptPath()
        val decline = mView?.getDeclinePath()
        val audioPath = mView?.getAudioPath()
        val audioName = mView?.getAudioName()
        prefs.apply {
            saveString(DIALER_BACKGROUND, background)
            saveString(DIALER_AVATAR, avatar)
            saveString(DIALER_ACCEPT, accept)
            saveString(DIALER_DECLINE, decline)
            saveString(DIALER_AUDIO_PATH, audioPath)
            saveString(DIALER_AUDIO_NAME, audioName)
        }
    }

    override fun saveApplyDialerScreenWithBackgroundUriFromUrl() {
        val background = mView?.getBackgroundPathUriFromUrlNotDownload()
        val avatar = mView?.getAvatarPath()
        val accept = mView?.getAcceptPath()
        val decline = mView?.getDeclinePath()
        val audioPath = mView?.getAudioPath()
        val audioName = mView?.getAudioName()
        prefs.apply {
            saveString(DIALER_BACKGROUND, background)
            saveString(DIALER_AVATAR, avatar)
            saveString(DIALER_ACCEPT, accept)
            saveString(DIALER_DECLINE, decline)
            saveString(DIALER_AUDIO_PATH, audioPath)
            saveString(DIALER_AUDIO_NAME, audioName)
        }
    }

    override fun insertDialerScreen(screen: DialerScreen) {
        callerDB.getCallerDao().insertDialerScreen(screen)
            .observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Long> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onSuccess(t: Long) {
                    mView?.insertSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }

    override fun isFileExist(path: String): Boolean {
        return File("${context.filesDir}/image/${path.substring(path.lastIndexOf("/") + 1)}").exists()
    }

    override fun getImageFileNameDownloaded(url: String): String {
        return File("${context.filesDir}/image/${url.substring(url.lastIndexOf("/") + 1)}").toString()
    }

    override fun downloadImageUrl(url: String) {
        mView?.showLoading(true)
        mFileHelper.save(url,
            onFailure = {
                mView?.showLoading(false)
            },
            onSuccess = {
                backgroundFileName = mFileHelper.urlImageToFile(url).toString()
                mView?.showLoading(false)
                mView?.getBackgroundPathUriFromUrlNotDownload()
                Log.i("aaa", mFileHelper.urlImageToFile(url).toString())
                mView?.onDownloadSuccess(mFileHelper.urlImageToFile(url).toString())
            })
    }

    override fun getBitmapFromUri(uriPath: String): Bitmap {
        return if(Build.VERSION.SDK_INT >= 29) {
            val source = ImageDecoder.createSource(context.contentResolver, Uri.parse(uriPath))
            ImageDecoder.decodeBitmap(source)
        } else {
            MediaStore.Images.Media.getBitmap(context.contentResolver, Uri.parse(uriPath))
        }
    }

    override fun saveBitmap(bitmap: Bitmap) {
        context.saveBitmap(bitmap)
    }

    override fun getBitmapPath(bitmap: Bitmap): String {
        return context.saveBitmap(bitmap)
    }

    override fun attach(view: PreviewContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}