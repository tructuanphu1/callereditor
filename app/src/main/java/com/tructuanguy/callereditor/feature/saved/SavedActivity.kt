package com.tructuanguy.callereditor.feature.saved

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.databinding.ActivitySavedBinding
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.helper.ImageHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SavedActivity : BaseActivity<ActivitySavedBinding>(), SavedContract.View {

    @Inject
    lateinit var mPresenter: SavedPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.showAllDialerScreen()
        mPresenter.applyCurrentDialerScreen()
    }

    override fun initViews() {
        binding.apply {
            back.setOnClickListener {
                finish()
            }
        }
    }


    override fun getLayout(): ActivitySavedBinding {
        return ActivitySavedBinding.inflate(layoutInflater)
    }

    override fun showEmpty() {
        binding.animEmpty.visible()
        binding.tvNotifyEmpty.visible()
        binding.rcvSavedScreen.gone()
        binding.tvAddNew.visible()
    }

    override fun hideEmpty() {
        binding.animEmpty.gone()
        binding.tvNotifyEmpty.gone()
        binding.tvAddNew.gone()
        binding.rcvSavedScreen.visible()
    }

    override fun displayBackgroundAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(path), binding.currentScreen.ivBackground)
    }

    override fun displayBackgroundLocal(path: String) {
        ImageHelper(this).loadUri(path, binding.currentScreen.ivBackground)
    }


    override fun displayAvatarAsset(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(AVATAR_ASSET_PATH, path), binding.currentScreen.ivAvatar)
    }

    override fun displayAvatarLocal(path: String) {
        ImageHelper(this).loadUri(path, binding.currentScreen.ivAvatar)
    }

    override fun displayAccept(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(ACCEPT_ASSET_PATH, path), binding.currentScreen.accept)
    }

    override fun displayDecline(path: String) {
        ImageHelper(this).loadBitmap(getBitmapFromAsset(DECLINE_ASSET_PATH, path), binding.currentScreen.decline)
    }

    @SuppressLint("SetTextI18n")
    override fun displayAudioName(name: String) {
        binding.tvAudio.text = "${getString(R.string.ringtone)}$name"
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun updateDialerScreenAdapter(list: MutableList<DialerScreen>) {
        lateinit var adapter: SavedScreenAdapter
        val handleApply: (DialerScreen, Int) -> Unit = { screen, _ ->
            mPresenter.saveApplyDialerScreen(screen)
            mPresenter.applyCurrentDialerScreen()
        }
        val handleDelete: (DialerScreen, Int) -> Unit = { screen, pos ->
            mPresenter.deleteDialerScreen(screen.id)
            adapter.notifyItemRemoved(pos)
        }
        adapter = SavedScreenAdapter(list, handleApply, handleDelete)
        Log.i("@@", list.toString())
        binding.rcvSavedScreen.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun showError(error: String) {
        Log.i("error", error)
    }

    override fun deleteSuccess() {
        shortToast(getString(R.string.delete_successfully))
        mPresenter.showAllDialerScreen()
    }

    override fun applySuccess() {
        shortToast(getString(R.string.apply_successfully))
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}