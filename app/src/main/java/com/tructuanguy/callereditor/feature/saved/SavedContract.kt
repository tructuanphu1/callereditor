package com.tructuanguy.callereditor.feature.saved

import com.tructuanguy.callereditor.core.ActivityContract
import com.tructuanguy.callereditor.db.entities.DialerScreen

interface SavedContract {

    interface View: ActivityContract.View {
        fun showEmpty()
        fun hideEmpty()
        fun displayBackgroundAsset(path: String)
        fun displayBackgroundLocal(path: String)
        fun displayAvatarAsset(path: String)
        fun displayAvatarLocal(path: String)
        fun displayAccept(path: String)
        fun displayDecline(path: String)
        fun displayAudioName(name: String)
        fun updateDialerScreenAdapter(list: MutableList<DialerScreen>)
        fun showError(error: String)
        fun deleteSuccess()
        fun applySuccess()
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun getBackgroundPath(): String
        fun getAvatarPath(): String
        fun getAcceptPath(): String
        fun getDeclinePath(): String
        fun getAudioPath(): String
        fun getAudioName(): String
        fun getAllDialerScreen(): List<DialerScreen>
        fun showAllDialerScreen()
        fun applyCurrentDialerScreen()
        fun deleteDialerScreen(id: Int)
        fun saveApplyDialerScreen(screen: DialerScreen)
    }

}