package com.tructuanguy.callereditor.feature.saved

import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_ACCEPT
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_NAME
import com.tructuanguy.callereditor.core.Constants.DIALER_AUDIO_PATH
import com.tructuanguy.callereditor.core.Constants.DIALER_AVATAR
import com.tructuanguy.callereditor.core.Constants.DIALER_BACKGROUND
import com.tructuanguy.callereditor.core.Constants.DIALER_DECLINE
import com.tructuanguy.callereditor.db.CallerDatabase
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.helper.PreferencesHelper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class SavedPresenter @Inject constructor(
    private val prefs: PreferencesHelper,
    private val callerDB: CallerDatabase
) : SavedContract.Presenter {

    private var mView: SavedContract.View? = null

    override fun attach(view: SavedContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }

    override fun getBackgroundPath(): String {
        return prefs.getString(DIALER_BACKGROUND).toString()
    }

    override fun getAvatarPath(): String {
        return prefs.getString(DIALER_AVATAR).toString()
    }

    override fun getAcceptPath(): String {
        return prefs.getString(DIALER_ACCEPT).toString()
    }

    override fun getDeclinePath(): String {
        return prefs.getString(DIALER_DECLINE).toString()
    }

    override fun getAudioPath(): String {
        return prefs.getString(DIALER_AUDIO_PATH).toString()
    }

    override fun getAudioName(): String {
        return prefs.getString(DIALER_AUDIO_NAME).toString()
    }


    override fun getAllDialerScreen(): List<DialerScreen> {
        try {
            return callerDB.getCallerDao().getAllDialerScreen()
        } catch (e: Exception) {
            mView?.showError(e.toString())
        }
        return emptyList()
    }

    override fun showAllDialerScreen() {
        val list = getAllDialerScreen()
        if (list.isEmpty()) {
            mView?.showEmpty()
        } else {
            mView?.hideEmpty()
            mView?.updateDialerScreenAdapter(list.toMutableList())
        }

    }

    override fun applyCurrentDialerScreen() {
        val background = getBackgroundPath()
        val avatar = getAvatarPath()
        val accept = getAcceptPath()
        val decline = getDeclinePath()
//        val audioPath = getAudioPath()
        val audioName = getAudioName()
        if (background.isNotEmpty()) {
            if (background.contains(APP_PATH)) {
                mView?.displayBackgroundLocal(background)
            } else {
                mView?.displayBackgroundAsset(background)
            }
        }
        if (avatar.isNotEmpty()) {
            if (avatar.contains(APP_PATH)) mView?.displayAvatarLocal(avatar) else mView?.displayAvatarAsset(
                avatar
            )
        }
        if (accept.isNotEmpty()) {
            mView?.displayAccept(accept)
        }
        if (decline.isNotEmpty()) {
            mView?.displayDecline(decline)
        }
        if (audioName.isNotEmpty()) {
            mView?.displayAudioName(audioName)
        }
    }

    override fun deleteDialerScreen(id: Int) {
        callerDB.getCallerDao().deleteDialerScreen(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onComplete() {
                    mView?.deleteSuccess()
                }

                override fun onError(e: Throwable) {
                    mView?.showError(e.toString())
                }
            })
    }


    override fun saveApplyDialerScreen(screen: DialerScreen) {
        prefs.apply {
            saveString(DIALER_BACKGROUND, screen.background)
            saveString(DIALER_AVATAR, screen.avatar)
            saveString(DIALER_ACCEPT, screen.accept)
            saveString(DIALER_DECLINE, screen.decline)
            saveString(DIALER_AUDIO_PATH, screen.audioPath)
            saveString(DIALER_AUDIO_NAME, screen.audioName)
        }
    }
}