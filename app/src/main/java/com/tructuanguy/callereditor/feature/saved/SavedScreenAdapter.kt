package com.tructuanguy.callereditor.feature.saved

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.URI_SIGNAL
import com.tructuanguy.callereditor.databinding.ItemSavedScreenBinding
import com.tructuanguy.callereditor.db.entities.DialerScreen
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.extensions.gone
import com.tructuanguy.callereditor.extensions.visible
import com.tructuanguy.callereditor.helper.ImageHelper

class SavedScreenAdapter(
    private val list: MutableList<DialerScreen>,
    private val applyListener: (DialerScreen, Int) -> Unit,
    private val deleteListener: (DialerScreen, Int) -> Unit
) : RecyclerView.Adapter<SavedScreenAdapter.SavedScreenHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION
    private var isChosen = false

    inner class SavedScreenHolder(private val binding: ItemSavedScreenBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(dialerScreen: DialerScreen) {
            binding.apply {
                if (dialerScreen.background.contains(URI_SIGNAL)) {
                    ImageHelper(itemView.context).loadUri(dialerScreen.background, ivBackground)
                } else if(dialerScreen.background.contains(APP_PATH)){
                    ImageHelper(itemView.context).loadUri(dialerScreen.background, ivBackground)
                }
                if (dialerScreen.avatar.contains(APP_PATH)) {
                    ImageHelper(itemView.context).loadUri(dialerScreen.avatar, ivAvatar)
                } else {
                    ImageHelper(itemView.context).loadBitmap(itemView.context.getBitmapFromAsset(AVATAR_ASSET_PATH, dialerScreen.avatar), ivAvatar)
                }
                ImageHelper(itemView.context).loadBitmap(
                    itemView.context.getBitmapFromAsset(
                        ACCEPT_ASSET_PATH, dialerScreen.accept
                    ), accept
                )
                ImageHelper(itemView.context).loadBitmap(
                    itemView.context.getBitmapFromAsset(DECLINE_ASSET_PATH, dialerScreen.decline), decline)
            }
            itemView.setOnClickListener {
                setChosen(adapterPosition)
                isChosen = true
            }
            if (adapterPosition == selectedPosition) {
                binding.cvSelection.visible()
                binding.apply.setOnClickListener {
                    applyListener.invoke(dialerScreen, adapterPosition)
                }
                binding.delete.setOnClickListener {
                    deleteListener.invoke(dialerScreen, adapterPosition)
                }
            } else {
                binding.cvSelection.gone()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedScreenHolder {
        return SavedScreenHolder(
            ItemSavedScreenBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SavedScreenHolder, position: Int) {
        holder.bind(list[position])
    }

    private fun setChosen(position: Int) {
        val previousSelectedPosition = selectedPosition
        selectedPosition = position
        for (index in 0 until list.size) {
            isChosen = (index == position)
        }
        notifyItemChanged(previousSelectedPosition)
        notifyItemChanged(selectedPosition)
    }

}