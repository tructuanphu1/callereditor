package com.tructuanguy.callereditor.feature.setting
import android.os.Bundle
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.databinding.ActivitySettingBinding
import com.tructuanguy.callereditor.extensions.launchActivity
import com.tructuanguy.callereditor.feature.language.LanguageActivity
import com.tructuanguy.callereditor.helper.FlashHelper
import com.tructuanguy.callereditor.helper.PermissionHelper
import com.tructuanguy.callereditor.helper.PreferencesHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingActivity : BaseActivity<ActivitySettingBinding>(), SettingContract.View {

    @Inject
    lateinit var mPresenter: SettingContract.Presenter

    @Inject
    lateinit var prefHelper: PreferencesHelper

    @Inject
    lateinit var mPermissionHelper: PermissionHelper

    @Inject
    lateinit var mFlashHelper: FlashHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
    }

    override fun initViews() {
        binding.apply {
            permission.setOnClickListener {
                showRequestDetailSettingsDialog()
            }
            back.setOnClickListener {
                onBackPressedDispatcher.onBackPressed()
            }
            language.setOnClickListener {
                launchActivity<LanguageActivity> {  }
            }
            switchVibration.isChecked = mPresenter.isVibrationOn()
            switchFlash.isChecked = mPresenter.isFlashOn()


            switchVibration.setOnCheckedChangeListener { _,isChecked ->
                if(isChecked) {
                    mPresenter.turnVibration(true)
                } else {
                    mPresenter.turnVibration(false)
                }
            }
            switchFlash.setOnCheckedChangeListener { _, isChecked ->
                if(isChecked) mPresenter.turnFlash(true) else mPresenter.turnFlash(false)
            }
        }
    }


    override fun getLayout(): ActivitySettingBinding {
        return ActivitySettingBinding.inflate(layoutInflater)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }

}