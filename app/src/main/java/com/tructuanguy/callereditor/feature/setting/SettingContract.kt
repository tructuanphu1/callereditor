package com.tructuanguy.callereditor.feature.setting

import com.tructuanguy.callereditor.core.ActivityContract

interface SettingContract {

    interface View: ActivityContract.View

    interface Presenter: ActivityContract.Presenter<View> {
        fun turnVibration(isTurn: Boolean)
        fun isVibrationOn(): Boolean
        fun turnFlash(isTurn: Boolean)
        fun isFlashOn(): Boolean
    }

}