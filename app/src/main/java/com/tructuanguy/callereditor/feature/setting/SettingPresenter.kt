package com.tructuanguy.callereditor.feature.setting

import com.tructuanguy.callereditor.const_app.PrefsConstants.PREF_FLASH
import com.tructuanguy.callereditor.const_app.PrefsConstants.PREF_VIBRATION
import com.tructuanguy.callereditor.helper.PreferencesHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

class SettingPresenter @Inject constructor(private val prefs: PreferencesHelper) : SettingContract.Presenter {

    private var mView: SettingContract.View? = null
    
    override fun turnVibration(isTurn: Boolean) {
        prefs.saveBoolean(PREF_VIBRATION, isTurn)
    }

    override fun isVibrationOn(): Boolean {
        return prefs.getBoolean(PREF_VIBRATION)
    }

    override fun turnFlash(isTurn: Boolean) {
        prefs.saveBoolean(PREF_FLASH, isTurn)
    }

    override fun isFlashOn(): Boolean {
        return prefs.getBoolean(PREF_FLASH)
    }

    override fun attach(view: SettingContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }

}