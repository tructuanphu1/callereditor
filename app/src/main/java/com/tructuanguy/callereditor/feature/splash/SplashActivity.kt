package com.tructuanguy.callereditor.feature.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.tructuanguy.callereditor.feature.edit.EditActivity
import com.tructuanguy.callereditor.core.BaseActivity
import com.tructuanguy.callereditor.databinding.ActivitySplashBinding
import com.tructuanguy.callereditor.extensions.launchActivity
import com.tructuanguy.callereditor.extensions.shortToast
import com.tructuanguy.callereditor.feature.introduce.IntroduceActivity
import com.tructuanguy.callereditor.feature.prefs.IsFirstInAppPrefs
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding>(), SplashContract.View {


    @Inject
    lateinit var mPresenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attach(this)
        mPresenter.workWithFirstInAppFlag()
    }


    override fun initViews() {
    }

    private fun goToIntroduceActivity() {
        launchActivity<IntroduceActivity> { }
    }

    private fun goToMainActivity() {
        launchActivity<EditActivity> { }
    }

    override fun getLayout(): ActivitySplashBinding =
        ActivitySplashBinding.inflate(layoutInflater)

    override fun isFirstInApp() {
        Handler(mainLooper).postDelayed({
            goToIntroduceActivity()
            finish()
        }, 2500L)
    }

    override fun isNotFirstInApp() {
        Handler(mainLooper).postDelayed({
            goToMainActivity()
            finish()
        }, 2500L)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }


}