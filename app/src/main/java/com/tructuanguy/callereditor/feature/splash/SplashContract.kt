package com.tructuanguy.callereditor.feature.splash

import com.tructuanguy.callereditor.core.ActivityContract

interface SplashContract {

    interface View: ActivityContract.View {
        fun isFirstInApp()
        fun isNotFirstInApp()
    }

    interface Presenter: ActivityContract.Presenter<View> {
        fun getFirstInAppFlag(): Boolean
        fun workWithFirstInAppFlag()
    }

}