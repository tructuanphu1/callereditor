package com.tructuanguy.callereditor.feature.splash

import android.util.Log
import com.tructuanguy.callereditor.core.Constants.IS_FIRST_IN_APP
import com.tructuanguy.callereditor.helper.PreferencesHelper
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    private val mPrefs: PreferencesHelper
) : SplashContract.Presenter {

    private var mView: SplashContract.View? = null

    override fun getFirstInAppFlag(): Boolean {
        return !mPrefs.contain(IS_FIRST_IN_APP) || mPrefs.getBoolean(IS_FIRST_IN_APP)
    }

    override fun workWithFirstInAppFlag() {
        if(getFirstInAppFlag()) {
            mView?.isFirstInApp()
        } else {
            mView?.isNotFirstInApp()
        }
    }

    override fun attach(view: SplashContract.View) {
        mView = view
    }

    override fun detach() {
        mView = null
    }


}