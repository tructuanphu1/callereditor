package com.tructuanguy.callereditor.helper

import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import android.telecom.Call
import android.telecom.Call.Callback
import android.telecom.InCallService
import android.telecom.VideoProfile
import android.util.Log
import com.tructuanguy.callereditor.R
import javax.inject.Inject

class CallHelper @Inject constructor(
    private val context: Context
){

    var mCall: Call? = null
    var mInCallService: InCallService? = null
    var missingCall = true

    fun accept() {
        mCall?.answer(VideoProfile.STATE_AUDIO_ONLY)
    }

    fun decline() {
        if(mCall != null) {
            if(mCall!!.state == Call.STATE_RINGING) {
                mCall!!.reject(false, null)
            } else {
                mCall!!.disconnect()
            }
        }
        missingCall = false
    }

    fun registerCallback(callback: Call.Callback) {
        if(mCall != null) {
            mCall!!.registerCallback(callback)
            missingCall = true
        }
    }

    fun unregisterCallback(callback: Callback) {
        mCall!!.unregisterCallback(callback)
    }

    fun getState() = if(mCall == null) {
        Call.STATE_DISCONNECTED
    } else {
        Log.i("state", mCall!!.state.toString())
    }

    fun getPhoneNumber(): String {
        return mCall?.details?.handle?.let {
            val uri = Uri.decode(it.toString())
            return@let uri.substringAfter("tel:")
        } ?: let { context.getString(R.string.unknown) }
    }

    fun getNameByNumber(number: String? = getPhoneNumber()): String {
        val uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number))
        val projection = arrayOf(ContactsContract.PhoneLookup.DISPLAY_NAME)
        var name = ""
        val cursor = context.contentResolver?.query(uri, projection, null, null, null)
        cursor?.run {
            if(moveToFirst()) {
                name = getString(0)
            }
            close()
        }
        return name.ifEmpty { context.getString(R.string.unknown) }
    }

}