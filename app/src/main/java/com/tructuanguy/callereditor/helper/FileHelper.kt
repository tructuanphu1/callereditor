package com.tructuanguy.callereditor.helper

import android.content.Context
import android.util.Log
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class FileHelper @Inject constructor(
    private val context: Context
) {

    companion object {
        const val IMAGE_OK = 1
    }

    private val imageDirection = File("${context.filesDir}/image")

    private suspend fun downloadImage(url: String, filePath: File = imageDirection): Int {
        if(File(filePath.toString(), getNameFromUrl(url)).exists()) {
            return IMAGE_OK
        }
        return suspendCoroutine { continuation ->
            PRDownloader.download(url, filePath.toString(), getNameFromUrl(url)).build()
                .setOnProgressListener {
                    Log.d("progress", "${it.currentBytes} / ${it.totalBytes}")
                }
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        continuation.resume(IMAGE_OK)
                    }

                    override fun onError(error: Error?) {
                        error?.connectionException?.let { continuation.resumeWithException(it) }
                        PRDownloader.cleanUp(1)
                    }
                })
        }
    }

    private fun getNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    fun urlImageToFile(url: String): File {
        return File(imageDirection, getNameFromUrl(url))
    }

    fun save(imageUrl: String, onSuccess: (number: Int) -> Unit, onFailure: () -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val imageResult = async { downloadImage(imageUrl) }
            if(imageResult.await() == 0) {
                withContext(Dispatchers.Main) {
                    onFailure
                }
            } else {
                withContext(Dispatchers.Main) {
                    onSuccess(imageResult.await())
                }
            }
        }
    }

}