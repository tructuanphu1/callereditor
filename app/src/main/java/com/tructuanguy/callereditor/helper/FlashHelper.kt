package com.tructuanguy.callereditor.helper

import android.content.Context
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject

class FlashHelper @Inject constructor(
    private val context: Context
) {

    private val blinkTime = 500L
    private val cameraManager: CameraManager by lazy {
        context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private var job: Job? = null

    private fun getCamera(): String? {
        cameraManager.cameraIdList.forEach {
            if (checkFlashAvailable(it)) {
                return it
            }
        }
        return null
    }

    private fun checkFlashAvailable(cameraId: String): Boolean {
        cameraManager.getCameraCharacteristics(cameraId).get(CameraCharacteristics.FLASH_INFO_AVAILABLE)?.let {
            return it
        }
        return false
    }

    fun flashOn() {
        getCamera()?.let { camera ->
            job = CoroutineScope(Dispatchers.IO).launch {
                var turn = false
                while (isActive) {
                    cameraManager.setTorchMode(camera, turn)
                    delay(blinkTime)
                    turn = !turn
                }
            }
        }
    }

    fun flashOff() {
        getCamera()?.let { camera ->
            job?.cancel()
            job = null
            cameraManager.setTorchMode(camera, false)
        }
    }



}