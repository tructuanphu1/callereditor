package com.tructuanguy.callereditor.helper

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import javax.inject.Inject

class ImageHelper @Inject constructor(private val context: Context) {

    fun loadUri(path: String, image: ImageView) {
        image.setImageURI(Uri.parse(path))
    }

    fun loadUrl(path: String, image: ImageView) {
        Glide.with(context).load(path).into(image)
    }

    fun loadBitmap(bitmap: Bitmap, image: ImageView) {
        Glide.with(context).asBitmap().load(bitmap).into(image)
    }

    fun loadResource(source: Int, image: ImageView) {
        image.setImageResource(source)
    }

}