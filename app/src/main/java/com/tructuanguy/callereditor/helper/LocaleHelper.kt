package com.tructuanguy.callereditor.helper

import android.app.Activity
import android.content.Context
import com.tructuanguy.callereditor.const_app.PrefsConstants.LANGUAGE
import com.tructuanguy.callereditor.feature.language.model.TypeOfLanguage
import java.util.Locale
import javax.inject.Inject

class LocaleHelper @Inject constructor(
    private val context: Context,
    private val mPrefs: PreferencesHelper
) {

    fun getLocale(context: Context): Locale {
        return when(mPrefs.getString(LANGUAGE)) {
            TypeOfLanguage.ENGLISH.toString() -> Locale.ENGLISH
            TypeOfLanguage.VIETNAMESE.toString() -> Locale("vi")
            else -> Locale.ENGLISH
        }
    }

    private fun updateLocale(context: Context, locale: Locale?): Context {
        val res = context.resources
        val config = res.configuration
        Locale.setDefault(locale ?: Locale.ENGLISH)
        Locale.getDefault()
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }

    fun updateLanguage(activity: Activity, locale: Locale? = getLocale(activity.baseContext)) {
        val res = activity.baseContext.resources
        val config = res.configuration
        Locale.setDefault(locale ?: Locale.ENGLISH)
        config.setLocale(locale ?: Locale.ENGLISH)
    }

}