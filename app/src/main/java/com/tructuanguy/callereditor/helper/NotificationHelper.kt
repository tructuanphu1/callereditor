package com.tructuanguy.callereditor.helper

import android.Manifest
import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.tructuanguy.callereditor.R
import com.tructuanguy.callereditor.core.Constants.ACCEPT_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.ACCEPT_CALL
import com.tructuanguy.callereditor.core.Constants.APP_PATH
import com.tructuanguy.callereditor.core.Constants.AVATAR_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_ASSET_PATH
import com.tructuanguy.callereditor.core.Constants.DECLINE_CALL
import com.tructuanguy.callereditor.extensions.getBitmapFromAsset
import com.tructuanguy.callereditor.feature.dialer.DialerActivity
import com.tructuanguy.callereditor.receiver.CallStateReceiver
import javax.inject.Inject

class NotificationHelper @Inject constructor(
    private val context: Context,
    private val notificationManager: NotificationManagerCompat
) {

    companion object {
        const val DIALER_CHANNEL_ID = "DIALER_CHANNEL_ID"
        const val DIALER_NOTIFICATION_ID = -1
        const val DIALER_NOTIFICATION_GROUP = "DIALER_NOTIFICATION_GROUP"
    }

    fun showDialerNotification(
        name: String = context.getString(R.string.unknown),
        phone: String = context.getString(R.string.unknown),
        avatar: String, accept: String, decline: String
    ) {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        notificationManager.notify(
            DIALER_NOTIFICATION_ID,
            createComingDialerNotification(name, phone, avatar, accept, decline)
        )
    }

    fun showReceivedDialer(name: String, number: String, avatar: String, accept: String, decline: String) {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        notificationManager.notify(DIALER_NOTIFICATION_ID, createReceiveCallNotification(name, number, avatar, accept, decline))
    }

    private fun createInComingCallRemoteView(
        name: String,
        phone: String,
        avatar: String,
        accept: String,
        decline: String,
        hideView: Boolean = false
    ): RemoteViews {
        val acceptCallIntent = Intent(context, CallStateReceiver::class.java)
        acceptCallIntent.action = ACCEPT_CALL
        val acceptPendingIntent = PendingIntent.getBroadcast(
            context, 0, acceptCallIntent,
            PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val rejectCallIntent = Intent(context, CallStateReceiver::class.java)
        rejectCallIntent.action = DECLINE_CALL
        val rejectPendingIntent = PendingIntent.getBroadcast(
            context, 1, rejectCallIntent,
            PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        return RemoteViews(context.packageName, R.layout.notifi_dialer).apply {
            setTextViewText(R.id.tv_name, name)
            setTextViewText(R.id.tv_phone, phone)
            if(avatar.contains(APP_PATH)) {
                setImageViewUri(R.id.img_avatar, Uri.parse(avatar))
            } else {
                setImageViewBitmap(R.id.img_avatar, context.getBitmapFromAsset(AVATAR_ASSET_PATH, avatar))
            }
            setImageViewBitmap(R.id.accept, context.getBitmapFromAsset(ACCEPT_ASSET_PATH, accept))
            setImageViewBitmap(R.id.decline, context.getBitmapFromAsset(DECLINE_ASSET_PATH, decline))
            setOnClickPendingIntent(R.id.accept, acceptPendingIntent)
            setOnClickPendingIntent(R.id.decline, rejectPendingIntent)
            if (hideView) {
                setViewVisibility(R.id.accept, View.GONE)
            }
        }
    }

    private fun createComingDialerNotification(
        name: String,
        phone: String,
        avatar: String,
        accept: String,
        decline: String
    ): Notification {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(context, DIALER_CHANNEL_ID)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setCategory(Notification.CATEGORY_CALL)
                .setGroup(DIALER_NOTIFICATION_GROUP)
                .setSmallIcon(R.drawable.icon_call_button)
                .setContentIntent(createDialerIntent())
                .setCustomContentView(createInComingCallRemoteView(name, phone, avatar, accept, decline))
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setOngoing(true)
                .setSound(null)
                .build()
        } else {
            TODO("VERSION.SDK_INT < O")
        }
    }

    fun clearComingCallNotification() {
        notificationManager.cancel(DIALER_NOTIFICATION_ID)
    }

    private fun createReceiveCallNotification(
        name: String,
        phone: String,
        avatar: String,
        accept: String,
        decline: String
    ): Notification {
        return NotificationCompat.Builder(context, DIALER_CHANNEL_ID)
            .setSmallIcon(R.drawable.icon_call_button)
            .setContentTitle(name)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(createDialerIntent())
            .setGroup(DIALER_NOTIFICATION_GROUP)
            .setOngoing(true)
            .setCustomContentView(createInComingCallRemoteView(name, phone, avatar, accept, decline, true))
            .setCategory(Notification.CATEGORY_CALL)
            .setUsesChronometer(true)
            .build()
    }

    private fun createDialerIntent(): PendingIntent {
        val intent = Intent(context, DialerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_MUTABLE)
    }
}

fun Application.createNotificationChannels() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        //Channel1
        val comingCallChannel1 = NotificationChannel(
            NotificationHelper.DIALER_CHANNEL_ID,
            getString(R.string.coming_call_channel_name),
            NotificationManager.IMPORTANCE_DEFAULT
        )
        comingCallChannel1.setSound(null, null)
        comingCallChannel1.description = getString(R.string.comingCallChannelDescription)

        //Channel2
        val comingCallChannel2 = NotificationChannel(
            NotificationHelper.DIALER_CHANNEL_ID,
            getString(R.string.coming_call_channel_name),
            NotificationManager.IMPORTANCE_DEFAULT
        )
        comingCallChannel2.setSound(null, null)
        comingCallChannel2.description = getString(R.string.comingCallChannelDescription)

        val manager = getSystemService(NotificationManager::class.java)
        manager?.createNotificationChannel(comingCallChannel1)
        manager?.createNotificationChannel(comingCallChannel2)
    }
}