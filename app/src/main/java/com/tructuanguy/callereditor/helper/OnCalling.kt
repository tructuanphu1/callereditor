package com.tructuanguy.callereditor.helper

import android.telecom.Call
import android.telecom.VideoProfile
import io.reactivex.subjects.BehaviorSubject

object OnCalling {

    val state: BehaviorSubject<Int> = BehaviorSubject.create()

    private val callerCallback = object : Call.Callback() {
        override fun onStateChanged(call: Call?, newState: Int) {
            super.onStateChanged(call, newState)
            state.onNext(newState)
        }
    }

    var call: Call? = null
        set(value) {
            field?.unregisterCallback(callerCallback)
            value?.let {
                it.registerCallback(callerCallback)
                state.onNext(it.state)
            }
            field = value
        }

    fun accept() {
        if(call != null) {
            call!!.answer(VideoProfile.STATE_AUDIO_ONLY)
        }
    }

    fun decline() {
        if(call != null) {
            call!!.disconnect()
        }
    }

}