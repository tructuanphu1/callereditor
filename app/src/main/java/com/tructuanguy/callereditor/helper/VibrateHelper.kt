package com.tructuanguy.callereditor.helper

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject

class VibrateHelper @Inject constructor(
    private val context: Context
) {

    private val vibrateTime = 500L
    private var job: Job? = null

    private val vibrator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        val vibratorManager = context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
        vibratorManager.defaultVibrator
    } else {
        @Suppress("DEPRECATION")
        context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    private fun isVibrationSupported(): Boolean = vibrator.hasVibrator()

    fun vibrateOn() {
        if(isVibrationSupported()) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                job = CoroutineScope(Dispatchers.IO).launch {
                    var turn = false
                    while (isActive) {
                        vibrator.vibrate(
                            VibrationEffect.createOneShot(
                                vibrateTime,
                                VibrationEffect.DEFAULT_AMPLITUDE
                            )
                        )
                        delay(vibrateTime)
                        turn = !turn
                    }
                }
            } else {
                @Suppress("DEPRECATION")
                job = CoroutineScope(Dispatchers.IO).launch {
                    var turn = false
                    while (isActive) {
                        vibrator.vibrate(vibrateTime)
                        delay(vibrateTime)
                        turn = !turn
                    }
                }
            }
        }
    }

    fun vibrateOff() {
        if(isVibrationSupported()) {
            job?.cancel()
            job = null
            vibrator.cancel()
        }
    }

}