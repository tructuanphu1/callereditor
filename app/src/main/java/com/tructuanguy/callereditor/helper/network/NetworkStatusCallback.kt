package com.tructuanguy.callereditor.helper.network

interface NetworkStatusCallback {
    fun onStatusChange(isConnected: Boolean)
}