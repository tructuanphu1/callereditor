package com.tructuanguy.callereditor.helper.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest

class NetworkStatusReceiver : BroadcastReceiver() {

    private var networkStatusCallback: NetworkStatusCallback? = null
    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var networkCallback: ConnectivityManager.NetworkCallback

    fun initNetworkStatusCallback(networkStatusCallback: NetworkStatusCallback) {
        this.networkStatusCallback = networkStatusCallback
    }

    override fun onReceive(context: Context, intent: Intent?) {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkCallback = object :  ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                if(networkStatusCallback != null) {
                    networkStatusCallback?.onStatusChange(true)
                }
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                networkStatusCallback?.onStatusChange(false)
            }
        }

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
    }

    fun unregisterNetworkCallback() {
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }
}