package com.tructuanguy.callereditor.receiver

import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import com.tructuanguy.callereditor.core.BaseReceiver
import com.tructuanguy.callereditor.feature.dialer.DialerActivity

class CallReceiver : BaseReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        val extras = intent?.extras
        if(extras != null) {
            val state = extras.getString(TelephonyManager.EXTRA_STATE)
            val phoneNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)
            phoneNumber?.let {
                launchIntent(context!!, state, phoneNumber)
            }
        }
    }

    private fun launchIntent(context: Context, state: String?, number: String?) {
        val intent = Intent(context, DialerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
        intent.putExtra("STATE", state)
        intent.putExtra("PHONE", number)
        context.startActivity(intent)
    }

}