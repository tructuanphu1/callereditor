package com.tructuanguy.callereditor.receiver

import android.content.Context
import android.content.Intent
import com.tructuanguy.callereditor.core.BaseReceiver
import com.tructuanguy.callereditor.core.Constants.ACCEPT_CALL
import com.tructuanguy.callereditor.core.Constants.DECLINE_CALL
import com.tructuanguy.callereditor.helper.CallHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CallStateReceiver : BaseReceiver() {

    @Inject
    lateinit var mCallHelper: CallHelper

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        when(intent?.action) {
            ACCEPT_CALL -> mCallHelper.accept()
            DECLINE_CALL -> mCallHelper.decline()
        }
    }

}