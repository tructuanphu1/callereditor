package com.tructuanguy.callereditor.service

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.session.MediaController
import android.media.session.MediaSessionManager
import android.os.Build
import android.service.notification.NotificationListenerService
import android.telecom.TelecomManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.KeyEvent
import androidx.core.app.ActivityCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.reflect.Method
import javax.inject.Inject

class CallManager @Inject constructor(
    @ApplicationContext private val context: Context
) {

    private val audioManager: AudioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

    fun acceptCall() {
        try {
            val tm = context.getSystemService(Context.TELECOM_SERVICE) as TelecomManager
            if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                tm.acceptRingingCall()
            }
        } catch (e: Exception) {
            throughReceiver(context)
        }
    }

    private fun getTelephonyService(context: Context): ITelephony? {
        val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        try {
            val c = Class.forName(tm.javaClass.name)
            val m: Method = c.getDeclaredMethod("getITelephony")
            m.isAccessible = true
            return m.invoke(tm) as ITelephony
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun throughTelephonyService(context: Context) {
        val telephonyService: ITelephony? = getTelephonyService(context)
        if(telephonyService != null) {
            telephonyService.silenceRinger()
            telephonyService.answerRingingCall()
        }
    }

    private fun throughAudioManager() {
        val downEvent = KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK)
        val upEvent = KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK)
        audioManager.dispatchMediaKeyEvent(downEvent)
        audioManager.dispatchMediaKeyEvent(upEvent)
    }

    private fun throughReceiver(context: Context) {
        try {
            throughTelephonyService(context)
        } catch (e: Exception) {
            val broadcastConnected = ("HTC".equals(Build.MANUFACTURER, true)) && !audioManager.isWiredHeadsetOn
            if(broadcastConnected) {
                isHeadsetConnected(false, context)
            }
            try {
                Runtime.getRuntime().exec("input keyevent " + KeyEvent.KEYCODE_HEADSETHOOK)
            } catch (e: Exception) {
                throughHeadsetHook(context)
            } finally {
                if(broadcastConnected) {
                    isHeadsetConnected(false, context)
                }
            }
        }
    }

    private fun isHeadsetConnected(connected: Boolean, context: Context) {
        val intent = Intent(Intent.ACTION_HEADSET_PLUG)
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY)
        intent.putExtra("state", if(connected) 1 else 0)
        intent.putExtra("name", "sms")
        try {
            context.sendOrderedBroadcast(intent, null)
        } catch (e: Exception) {
            Log.i("isHeadsetConnected", e.toString())
        }
    }

    private fun throughHeadsetHook(context: Context) {
        val buttonDown = Intent(Intent.ACTION_MEDIA_BUTTON)
        buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK))
        context.sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED")
        val buttonUp = Intent(Intent.ACTION_MEDIA_BUTTON)
        buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK))
        context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED")
    }

    private fun throughMediaController(context: Context) {
        val msm: MediaSessionManager = context.getSystemService(Context.MEDIA_SESSION_SERVICE) as MediaSessionManager
        try {
            val controllers: List<MediaController> = msm.getActiveSessions(
                ComponentName(context, NotificationListenerService::class.java)
            )
            for(controller in controllers) {
                if("com.android.server.telecom" == controller.packageName) {
                    controller.dispatchMediaButtonEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK))
                    break
                }
            }
        } catch (e: Exception) {
            throughAudioManager()
        }
    }

}

interface ITelephony {
    fun silenceRinger()
    fun answerRingingCall()
}