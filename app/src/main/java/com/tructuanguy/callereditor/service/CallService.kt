package com.tructuanguy.callereditor.service

import android.content.Intent
import android.telecom.Call
import android.telecom.InCallService
import com.tructuanguy.callereditor.feature.dialer.DialerActivity
import com.tructuanguy.callereditor.helper.CallHelper
import com.tructuanguy.callereditor.helper.OnCalling
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CallService : InCallService() {


    override fun onCallAdded(call: Call?) {
        super.onCallAdded(call)
        OnCalling.call = call
        if(call != null) {
            val intent = Intent(this, DialerActivity::class.java)
            intent.setData(call.details.handle)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    override fun onCallRemoved(call: Call?) {
        super.onCallRemoved(call)
        OnCalling.call = null
    }
}